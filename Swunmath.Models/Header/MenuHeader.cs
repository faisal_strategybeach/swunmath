﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Swunmath.Models
{
    public class MenuHeader
    {
        public UInt64 Id { get; set; }
        public string PostTitle { get; set; }
        public string PostName { get; set; }
        public int MenuOrder { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string MenuParent { get; set; }
        public List<MenuHeader> LstMainMenu { get; set; }
        public List<MenuHeader> LstSubMenu { get; set; }
        public List<MenuHeader> LstSubSubMenu { get; set; }
    }
}