﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Swunmath.Models.Common
{
    public class HelpMethod
    {
        public string Method { get; set; }
        public string RelativePath { get; set; }
        public string ReturnType { get; set; }
        public IEnumerable<HelpParameter> Parameters { get; set; }
    }

    public class HelpParameter
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsOptional { get; set; }
    }
}