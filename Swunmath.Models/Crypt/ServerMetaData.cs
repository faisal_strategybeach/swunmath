﻿namespace Swunmath.Models
{
    public class ServerMetaData
    {
        public int Id { get; set; }
        public string EncryptionKey { get; set; }
        public string ServerIpAddress { get; set; }
        public string SshUser { get; set; }
        public string SshPassword { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string DbUser { get; set; }
        public string DbPassword { get; set; }
    }
}