﻿namespace Swunmath.Models
{
    public static class Crypt
    {
        /// <summary>
        /// Validate the WordPress password using CryptSharp
        /// </summary>
        /// <param name="password"></param>
        /// <param name="storePassword"></param>
        /// <returns>bool</returns>
        public static bool CheckPassword(string password,string storePassword)
        {
            return CryptSharp.PhpassCrypter.CheckPassword(password, storePassword);
        }
    }
}