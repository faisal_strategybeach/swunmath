﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Swunmath.Models
{
    public class UserSession
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public string access_token { get; set; }
        public DateTime expired { get; set; }

    }
}