﻿
namespace Swunmath.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string StoredPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? District { get; set; }
        public string Grade { get; set; }
    }
}