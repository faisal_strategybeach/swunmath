USE [Swunmath]
GO

/****** Object:  Table [dbo].[MasterTeacherPassword]    Script Date: 09/19/2020 16:17:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MasterTeacherPassword]') AND type in (N'U'))
DROP TABLE [dbo].[MasterTeacherPassword]
GO

USE [Swunmath]
GO

/****** Object:  Table [dbo].[MasterTeacherPassword]    Script Date: 09/19/2020 16:17:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MasterTeacherPassword](
	[Id] [Int] Identity,
	[MasterPassword] [varchar](50) NULL,
	PRIMARY KEY (ID)
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


