CREATE TABLE swun_math.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `district_id` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `last_ip` int(10) unsigned DEFAULT NULL,
  `grade_level` varchar(10) DEFAULT NULL,
  `logins` int(11) DEFAULT NULL,
  `c_time` int(11) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `video_grades` varchar(10) DEFAULT NULL,
  `last_reset` datetime DEFAULT '2014-01-01 01:01:01',
  `blocked_trimesters` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9685 DEFAULT CHARSET=utf8;


Insert into swun_math.users values('102', 'Gradetk@mailinator.com', '', '$2a$08$5ry6aOfxVTS3SC6IL6/CYeJY8vvrHa6ae5SjeFNQw/VKnZLv7ZZqG', '2018-08-03 11:52:39', '2019-12-03 07:50:41', 'Gradetk', 'Gradetk', '135', NULL, '2', NULL, NULL, NULL, NULL, '2014-01-01 01:01:01', NULL, NULL, NULL, '');

INSERT INTO swun_math.`users` VALUES (101,'cstein@lbschools.net','6','$2a$08$5ry6aOfxVTS3SC6IL6/CYeJY8vvrHa6ae5SjeFNQw/VKnZLv7ZZqG','2017-07-13 14:05:39','2017-08-02 17:20:56','Test user','Lastname',119,NULL,'A',NULL,NULL,'maths','K12345678','2017-07-16 12:14:06','123');
INSERT INTO swun_math.`users` VALUES (102,'manjula.rajan@pegasusone.com','6','$2a$08$5ry6aOfxVTS3SC6IL6/CYeJY8vvrHa6ae5SjeFNQw/VKnZLv7ZZqG','2017-07-13 14:05:39','2017-08-02 17:20:56','Test user','Lastname',119,NULL,'A',NULL,NULL,'maths','K12345678','2017-07-16 12:14:06','123');
INSERT INTO swun_math.`users` VALUES (103,'district6@mailinator.com','','$2a$08$5ry6aOfxVTS3SC6IL6/CYeJY8vvrHa6ae5SjeFNQw/VKnZLv7ZZqG','2017-07-13 14:05:39','2017-08-02 17:20:56','Test user','Lastname',6,NULL,'A',NULL,NULL,'maths','K12345678','2017-07-16 12:14:06','123');
INSERT INTO swun_math.`users` VALUES (104,'gradeTK@mailinator.com','','$2a$08$5ry6aOfxVTS3SC6IL6/CYeJY8vvrHa6ae5SjeFNQw/VKnZLv7ZZqG','2017-07-13 14:05:39','2017-08-02 17:20:56','Test user','Lastname',119,NULL,'TK',NULL,NULL,'maths','K12345678','2017-07-16 12:14:06','123');