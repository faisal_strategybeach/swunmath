CREATE TABLE swun_math.servermetadata (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EncryptionKey` varchar(255),
  `ServerIPAddress` varchar(15),
  `SshUser` varchar(15),  
  `SshPassword` varchar(255), 
  `Host` varchar(15), 
  `Port` varchar(15), 
  `DatabaseName` varchar(15), 
  `DbUser` varchar(15), 
  `DbPassword` varchar(255),
  PRIMARY KEY (`id`),
  UNIQUE KEY `ServerIPAddress` (`ServerIPAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

INSERT INTO swun_math.servermetadata values (101,'~0123456789*ABCDEFGHIJKLMNOPQRSTUVWXYZ%&#@^','45.33.48.166', 'root','aZI8D3+nuKNjmsNwTNfY+6vDo6yYjVmX2xwl9aZlIGg=', '127.0.0.1', '3306','swun_math', 'root', '9gCehHebbHKb4n7y2hn0bfrvlV2Gthl9Fd8ImY3KGDI=');

CREATE TABLE swun_math.`Grades` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Grade` varchar(255) DEFAULT NULL,
  `Status` bit DEFAULT 1,  
  `created_at` datetime DEFAULT NULL, 
  PRIMARY KEY (`id`),
  UNIQUE KEY `Grade` (`Grade`)
) ENGINE=InnoDB AUTO_INCREMENT=9685 DEFAULT CHARSET=utf8;

INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('TK', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('K', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('1', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('2', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('3', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('4', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('5', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('6', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('7', 1,curdate());
INSERT INTO swun_math.Grades (`Grade`, `Status`,`created_at`) VALUES ('8', 1,curdate());