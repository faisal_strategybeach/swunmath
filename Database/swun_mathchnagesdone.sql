select * from swun_math.config;
UPDATE `swun_math`.`config` SET `value` = 'https://localhost:8080/mathtools/' WHERE (`key` = 'siteWebpageAdress');

select * from swun_math.shares;
UPDATE `swun_math`.`shares` SET `value` = '<mxGraphModel><root><mxCell id=\\\"0\\\"/><mxCell id=\\\"1\\\" parent=\\\"0\\\"/></root></mxGraphModel><draw></draw><background><url>https://localhost:8080/mathtools/lib/stencils/background/blankCalendar.jpg</url><width>900</width><height>620</height></background>' WHERE (`id` = '78');
UPDATE `swun_math`.`shares` SET `value` = '<mxGraphModel><root><mxCell id=\\\"0\\\"/><mxCell id=\\\"1\\\" parent=\\\"0\\\"/></root></mxGraphModel><draw></draw>' WHERE (`id` = '79');

SELECT *
FROM swun_math.sm_postmeta
WHERE meta_value LIKE '%swunmath.com%';

UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'http://localhost:8080/students.php' WHERE (`meta_id` = '4855');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'http://localhost:8080/students.php' WHERE (`meta_id` = '4938');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'http://localhost:8080/student-videos/' WHERE (`meta_id` = '4947');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'http://localhost:8080/resources.php' WHERE (`meta_id` = '4986');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'http://localhost:8080/resources.php' WHERE (`meta_id` = '6949');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'https://localhost:8080/students' WHERE (`meta_id` = '5002');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'https://localhost:8080/student-videos/' WHERE (`meta_id` = '5011');
UPDATE `swun_math`.`sm_postmeta` SET `meta_value` = 'http://localhost:8080/resources' WHERE (`meta_id` = '5490');


SELECT REPLACE('a:2:{s:11:"layout_html";s:9347:"<div data-placeholder="1/2 Column" data-name="1_2" class="module m_column m_column_1_2 ui-draggable ui-draggable-handle ui-resizable ui-droppable ui-sortable first" style="opacity: 1; display: inline-block; z-index: 1;"><span class="module_name column_name">1/2 Column</span> <span class="delete_column delete" style="display: block;"></span><div data-placeholder="WP Editor" data-name="text_block" class="module m_text_block ui-draggable ui-draggable-handle ui-resizable first" style="opacity: 1; display: inline-block; width: 100%; margin-right: 0px; z-index: 1; height: 18px;"><span class="module_name">Content</span><span class="move" style="display: block;"></span><span class="delete" style="display: block;"></span><span class="settings_arrow"></span><div class="module_settings"><div data-option_name="title" class="title module_setting">Content</div><div data-option_name="text_block_content" class="text_block_content module_setting v_module_content"><h5><strong>We would love to hear it from you</strong></h5>
 <p>Curabitur lacinia tellus vitae quam pretium tincidunt. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in urna eu lorem porttitor rutrum. Duis tortor nisi, viverra et posuere non, sodales sit amet mi. Morbi vitae sodales augue. Praesent libero nisl, convallis ac cursus et, dictum et mi.</p>
 </div><div data-option_name="animation_effect" class="animation_effect module_setting"></div><div data-option_name="css_class" class="css_class module_setting"></div><div data-option_name="container_css" class="container_css module_setting"></div><div data-option_name="custom_css" class="custom_css module_setting"></div></div></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div></div><div data-placeholder="1/4 Column" data-name="1_4" class="module m_column m_column_1_4 ui-draggable ui-draggable-handle ui-resizable ui-droppable ui-sortable" style="opacity: 1; display: inline-block; z-index: 1;"><span class="module_name column_name">1/4 Column</span> <span class="delete_column delete" style="display: block;"></span><div data-placeholder="WP Editor" data-name="text_block" class="module m_text_block ui-draggable ui-draggable-handle" style="opacity: 1; display: inline-block; width: 100%; margin-right: 0px; z-index: 1;"><span class="module_name">Content</span><span class="move" style="display: block;"></span><span class="delete" style="display: block;"></span><span class="settings_arrow"></span><div class="module_settings"><div data-option_name="title" class="title module_setting">Content</div><div data-option_name="text_block_content" class="text_block_content module_setting v_module_content"><h5><strong>Our Philosophy</strong></h5>
 <p>Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</p></div><div data-option_name="animation_effect" class="animation_effect module_setting"></div><div data-option_name="css_class" class="css_class module_setting"></div><div data-option_name="container_css" class="container_css module_setting"></div><div data-option_name="custom_css" class="custom_css module_setting"></div></div></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div></div><div data-placeholder="1/4 Column" data-name="1_4" class="module m_column m_column_1_4 ui-draggable ui-draggable-handle ui-resizable ui-droppable ui-sortable" style="opacity: 1; display: inline-block; z-index: 1;"><span class="module_name column_name">1/4 Column</span> <span class="delete_column delete" style="display: block;"></span><div data-placeholder="WP Editor" data-name="text_block" class="module m_text_block ui-draggable ui-draggable-handle ui-resizable first" style="opacity: 1; display: inline-block; width: 100%; margin-right: 0px; z-index: 1; height: 18px;"><span class="module_name">Content</span><span class="move" style="display: block;"></span><span class="delete" style="display: block;"></span><span class="settings_arrow"></span><div class="module_settings"><div data-option_name="title" class="title module_setting">Content</div><div data-option_name="text_block_content" class="text_block_content module_setting v_module_content"><h5><strong>Contact Us</strong></h5><p>746, Arcade Ave</p><p>Near Madison Square</p><p>NYC</p><p>899.012.12.00</p></div><div data-option_name="animation_effect" class="animation_effect module_setting"></div><div data-option_name="css_class" class="css_class module_setting"></div><div data-option_name="container_css" class="container_css module_setting"></div><div data-option_name="custom_css" class="custom_css module_setting"></div></div></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div></div><div data-placeholder="1/2 Column" data-name="1_2" class="module m_column m_column_1_2 ui-draggable ui-draggable-handle ui-resizable ui-droppable ui-sortable first" style="opacity: 1; display: inline-block; z-index: 1;"><span class="module_name column_name">1/2 Column</span> <span class="delete_column delete" style="display: block;"></span><div data-placeholder="WP Editor" data-name="text_block" class="module m_text_block ui-draggable ui-draggable-handle ui-resizable first" style="opacity: 1; display: inline-block; width: 100%; margin-right: 0px; z-index: 1; height: 18px;"><span class="module_name">Content</span><span class="move" style="display: block;"></span><span class="delete" style="display: block;"></span><span class="settings_arrow"></span><div class="module_settings"><div data-option_name="title" class="title module_setting">Content</div><div data-option_name="text_block_content" class="text_block_content module_setting v_module_content"><h5><strong>Send us a message</strong></h5>
 <p>[form to="info@swunmath.com" subject="Subject"] [form_element type="text" validate="required" options="" placeholder="Name"] [form_element type="text" validate="required" options="" placeholder="Email"] [form_element type="text" validate="" options="" placeholder="Phone"] [form_element type="textarea" validate="" options="" placeholder="Query"] [form_element type="submit" validate="" options="" placeholder="Submit"] [/form]</p>
 </div><div data-option_name="animation_effect" class="animation_effect module_setting"></div><div data-option_name="css_class" class="css_class module_setting"></div><div data-option_name="container_css" class="container_css module_setting"></div><div data-option_name="custom_css" class="custom_css module_setting"></div></div></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div></div><div data-placeholder="1/2 Column" data-name="1_2" class="module m_column m_column_1_2 ui-draggable ui-draggable-handle ui-resizable ui-droppable ui-sortable" style="opacity: 1; display: inline-block; z-index: 1;"><span class="module_name column_name">1/2 Column</span> <span class="delete_column delete" style="display: block;"></span><div data-placeholder="WP Editor" data-name="text_block" class="module m_text_block ui-draggable ui-draggable-handle" style="opacity: 1; display: inline-block; width: 100%; margin-right: 0px; z-index: 1;"><span class="module_name">Content</span><span class="move" style="display: block;"></span><span class="delete" style="display: block;"></span><span class="settings_arrow"></span><div class="module_settings"><div data-option_name="title" class="title module_setting">Content</div><div data-option_name="text_block_content" class="text_block_content module_setting v_module_content"><p>[agroup first="1" connect="119"] [accordion title="Our mission" connect="119"] Curabitur lacinia tellus vitae quam pretium tincidunt. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [accordion title="Our Motto" connect="119"] Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [accordion title="Our Vision" connect="119"] Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [accordion title="Our Process" connect="119"] Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [/agroup]</p></div><div data-option_name="animation_effect" class="animation_effect module_setting"></div><div data-option_name="css_class" class="css_class module_setting"></div><div data-option_name="container_css" class="container_css module_setting"></div><div data-option_name="custom_css" class="custom_css module_setting"></div></div></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div></div>";s:16:"layout_shortcode";s:3266:"[v_1_2 first_class="1"]
 [v_text_block first_class="1" title="Content" animation_effect="" css_class="" container_css="" custom_css=""]<h5><strong>We would love to hear it from you</strong></h5>
 <p>Curabitur lacinia tellus vitae quam pretium tincidunt. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in urna eu lorem porttitor rutrum. Duis tortor nisi, viverra et posuere non, sodales sit amet mi. Morbi vitae sodales augue. Praesent libero nisl, convallis ac cursus et, dictum et mi.</p>
 [/v_text_block]
 [/v_1_2]
 [v_1_4]
 [v_text_block title="Content" animation_effect="" css_class="" container_css="" custom_css=""]<h5><strong>Our Philosophy</strong></h5>
 <p>Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</p>[/v_text_block]
 [/v_1_4]
 [v_1_4]
 [v_text_block first_class="1" title="Content" animation_effect="" css_class="" container_css="" custom_css=""]<h5><strong>Contact Us</strong></h5><p>746, Arcade Ave</p><p>Near Madison Square</p><p>NYC</p><p>899.012.12.00</p>[/v_text_block]
 [/v_1_4]
 [v_1_2 first_class="1"]
 [v_text_block first_class="1" title="Content" animation_effect="" css_class="" container_css="" custom_css=""]<h5><strong>Send us a message</strong></h5>
 <p>[form to="info@swunmath.com" subject="Subject"] [form_element type="text" validate="required" options="" placeholder="Name"] [form_element type="text" validate="required" options="" placeholder="Email"] [form_element type="text" validate="" options="" placeholder="Phone"] [form_element type="textarea" validate="" options="" placeholder="Query"] [form_element type="submit" validate="" options="" placeholder="Submit"] [/form]</p>
 [/v_text_block]
 [/v_1_2]
 [v_1_2]
 [v_text_block title="Content" animation_effect="" css_class="" container_css="" custom_css=""]<p>[agroup first="1" connect="119"] [accordion title="Our mission" connect="119"] Curabitur lacinia tellus vitae quam pretium tincidunt. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [accordion title="Our Motto" connect="119"] Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [accordion title="Our Vision" connect="119"] Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [accordion title="Our Process" connect="119"] Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum neque erat, sit amet volutpat enim luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. [/accordion] [/agroup]</p>[/v_text_block]
 [/v_1_2]
 ";}', "swunmath.com", "localhost:8080") FROM swun_math.sm_postmeta WHERE meta_id=5676;
 
 select * from swun_math.sm_posts;
 UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?p=1' WHERE (`ID` = '1');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?page_id=2' WHERE (`ID` = '2');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/activity/' WHERE (`ID` = '4');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/members/' WHERE (`ID` = '5');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=6' WHERE (`ID` = '6');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=7' WHERE (`ID` = '7');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=8' WHERE (`ID` = '8');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=9' WHERE (`ID` = '9');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=10' WHERE (`ID` = '10');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=11' WHERE (`ID` = '11');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=12' WHERE (`ID` = '12');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=13' WHERE (`ID` = '13');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=14' WHERE (`ID` = '14');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=15' WHERE (`ID` = '15');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=16' WHERE (`ID` = '16');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=17' WHERE (`ID` = '17');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=18' WHERE (`ID` = '18');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=19' WHERE (`ID` = '19');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=20' WHERE (`ID` = '20');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/?post_type=bp-email&p=21' WHERE (`ID` = '21');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/course-status/' WHERE (`ID` = '22');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/edit-course/' WHERE (`ID` = '23');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/all-courses/' WHERE (`ID` = '24');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/register/' WHERE (`ID` = '25');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/activate/' WHERE (`ID` = '26');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/activate-2/' WHERE (`ID` = '27');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/notes-discussion/' WHERE (`ID` = '28');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img1.jpg' WHERE (`ID` = '29');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img2.jpg' WHERE (`ID` = '30');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img3.jpg' WHERE (`ID` = '31');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img4.jpg' WHERE (`ID` = '32');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img5.jpg' WHERE (`ID` = '33');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img6.jpg' WHERE (`ID` = '34');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img7.jpg' WHERE (`ID` = '35');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img8.jpg' WHERE (`ID` = '36');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img9.jpg' WHERE (`ID` = '37');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img10.jpg' WHERE (`ID` = '38');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img11.jpg' WHERE (`ID` = '39');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2014/02/img12.jpg' WHERE (`ID` = '40');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/badge.png' WHERE (`ID` = '41');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/badge2.png' WHERE (`ID` = '42');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/notes_discussion.png' WHERE (`ID` = '43');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/create_course.jpg' WHERE (`ID` = '44');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/banner5.jpg' WHERE (`ID` = '45');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/sample.png' WHERE (`ID` = '46');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/thumbsup.png' WHERE (`ID` = '47');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/prof4.jpg' WHERE (`ID` = '48');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/prof3.jpg' WHERE (`ID` = '49');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/prof2.jpg' WHERE (`ID` = '50');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/prof1.jpg' WHERE (`ID` = '51');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/user1.jpg' WHERE (`ID` = '52');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/t2.jpg' WHERE (`ID` = '53');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2016/02/logo_black.png' WHERE (`ID` = '70');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img12.jpg' WHERE (`ID` = '69');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img11.jpg' WHERE (`ID` = '68');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img10.jpg' WHERE (`ID` = '67');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img9.jpg' WHERE (`ID` = '66');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img8.jpg' WHERE (`ID` = '65');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img7.jpg' WHERE (`ID` = '64');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img6.jpg' WHERE (`ID` = '63');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img5.jpg' WHERE (`ID` = '62');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img4.jpg' WHERE (`ID` = '61');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img3.jpg' WHERE (`ID` = '60');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img2.jpg' WHERE (`ID` = '59');
UPDATE `swun_math`.`sm_posts` SET `guid` = 'http://localhost:8080/wp-content/uploads/2008/09/img1.jpg' WHERE (`ID` = '58');

select * from swun_math.sm_options;
UPDATE `swun_math`.`sm_options` SET `option_value` = 'http://localhost:8080' WHERE (`option_id` = '1');
UPDATE `swun_math`.`sm_options` SET `option_value` = 'http://localhost:8080' WHERE (`option_id` = '2');
