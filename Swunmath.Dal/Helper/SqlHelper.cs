﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using Renci.SshNet;
using Swunmath.Models;

namespace Swunmath.DAL
{
    public class SqlHelper
    {

        public static long ExecuteNonQuery(string sqlQuery)
        {
            long id = 0;
            var dsQueryResult = new DataSet();
            var serverMetaData = GetServerMetaData();
            var sshPassword = Cryptography.Decrypt(serverMetaData.SshPassword, serverMetaData.EncryptionKey);
            var dbPassword = Cryptography.Decrypt(serverMetaData.DbPassword, serverMetaData.EncryptionKey);
            using (var client = new SshClient(serverMetaData.ServerIpAddress, serverMetaData.SshUser, sshPassword))
            {
                client.Connect();
                var tunnel = new ForwardedPortLocal(serverMetaData.Host, serverMetaData.Host, Convert.ToUInt16(serverMetaData.Port));
                client.AddForwardedPort(tunnel);
                tunnel.Start();
                var connectionStr = $"server={serverMetaData.Host};User Id={serverMetaData.DbUser};password={dbPassword};database={serverMetaData.Database};port={tunnel.BoundPort}";
                using (var conn = new MySqlConnection(connectionStr))
                {
                  
                    using (MySqlCommand cmd = new MySqlCommand(sqlQuery, conn))
                    {
                        conn.Open();
                        cmd.Prepare();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        id = cmd.LastInsertedId;
                    }
                  
                }
                tunnel.Stop();
            }
            return id;
        }

        public static DataSet ExecuteSqlQuery(string sqlQuery)
        {
            var dsQueryResult = new DataSet();
            var serverMetaData = GetServerMetaData();
            var sshPassword = Cryptography.Decrypt(serverMetaData.SshPassword, serverMetaData.EncryptionKey);
            var dbPassword = Cryptography.Decrypt(serverMetaData.DbPassword, serverMetaData.EncryptionKey);
            using (var client = new SshClient(serverMetaData.ServerIpAddress, serverMetaData.SshUser, sshPassword))
            {
                client.Connect();
                var tunnel = new ForwardedPortLocal(serverMetaData.Host, serverMetaData.Host, Convert.ToUInt16(serverMetaData.Port));
                client.AddForwardedPort(tunnel);
                tunnel.Start();
                var connectionStr = $"server={serverMetaData.Host};User Id={serverMetaData.DbUser};password={dbPassword};database={serverMetaData.Database};port={tunnel.BoundPort}";
                using (var conn = new MySqlConnection(connectionStr))
                {
                    var dataAdapter = new MySqlDataAdapter(sqlQuery, conn);
                    dataAdapter.Fill(dsQueryResult);
                }
                tunnel.Stop();
            }
            return dsQueryResult;
        }
        /// <summary>
        /// Get Menu Header
        /// Replace # with =0(to get Main menu) or in(745,746,788,755,1427,1380) => to get sub menus in sqlQuery
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
       public static MenuHeader GetMenus(string sqlQuery)
       {
           var menuHeader = new MenuHeader();
           var serverMetaData = GetServerMetaData();
           var sshPassword = Cryptography.Decrypt(serverMetaData.SshPassword, serverMetaData.EncryptionKey);
           var dbPassword = Cryptography.Decrypt(serverMetaData.DbPassword, serverMetaData.EncryptionKey);
           var menuQuery = sqlQuery.Replace("#", "=0");
          
           using (var client = new SshClient(serverMetaData.ServerIpAddress, serverMetaData.SshUser, sshPassword))
           {
               client.Connect();
               var tunnel = new ForwardedPortLocal(serverMetaData.Host, serverMetaData.Host, Convert.ToUInt16(serverMetaData.Port));
               client.AddForwardedPort(tunnel);
               tunnel.Start();
               var connectionStr = $"server={serverMetaData.Host};User Id={serverMetaData.DbUser};password={dbPassword};database={serverMetaData.Database};port={tunnel.BoundPort}";
               using (var conn = new MySqlConnection(connectionStr))
               {
                   var lstMainMenu = FillDataTable(menuQuery, conn);
                   if (lstMainMenu != null && lstMainMenu.Count > 0)
                   {
                      var parentMenuIds=string.Join(",", lstMainMenu.Select(x => x.Id.ToString()).ToArray());
                      menuQuery = sqlQuery.Replace("#", $"in ({parentMenuIds})");
                      menuHeader.LstSubMenu = FillDataTable(menuQuery, conn);
                      if (menuHeader.LstSubMenu != null && menuHeader.LstSubMenu.Count > 0)
                      {
                          parentMenuIds = string.Join(",", menuHeader.LstSubMenu.Select(x => x.Id.ToString()).ToArray());
                          menuQuery = sqlQuery.Replace("#", $"in ({parentMenuIds})");
                          menuHeader.LstSubSubMenu = FillDataTable(menuQuery, conn);
                      }
                      menuHeader.LstMainMenu = lstMainMenu;
                   }
               }
               tunnel.Stop();
           }
           return menuHeader;
       }
        
        private static List<MenuHeader> FillDataTable(string sqlQuery,MySqlConnection conn)
        {
            var dtMenu=new DataTable();
            var lstMenu = new List<MenuHeader>();
            var dataAdapter = new MySqlDataAdapter(sqlQuery, conn);
            dataAdapter.Fill(dtMenu);
            if (dtMenu.Rows.Count > 0)
            {
                lstMenu = Utility.ToList<MenuHeader>(dtMenu);
            }
            return lstMenu;
        }
       /// <summary>
       /// Get server details to connect with Client's My Sql server with SSH
       /// </summary>
       /// <returns></returns>
       private static ServerMetaData GetServerMetaData()
       {
           var serverMetaData = new ServerMetaData()
           {
               EncryptionKey = ConfigurationManager.AppSettings["EncryptionKey"],
               ServerIpAddress = ConfigurationManager.AppSettings["ServerIpAddress"],
               SshUser = ConfigurationManager.AppSettings["SshUser"],
               SshPassword = ConfigurationManager.AppSettings["SshPassword"],
               Host = ConfigurationManager.AppSettings["Host"],
               Port = ConfigurationManager.AppSettings["Port"],
               Database = ConfigurationManager.AppSettings["Database"],
               DbUser = ConfigurationManager.AppSettings["DbUser"],
               DbPassword = ConfigurationManager.AppSettings["DbPassword"]
           };
           return serverMetaData;
       }
    }

    public static class Utility
    {
        #region UtilityMethod
        /// <summary>
        /// Converts the specified value to <code>DBNull.Value</code> if it is <code>null</code>.
        /// </summary>
        /// <param name="value">The value that should be checked for <code>null</code>.</param>
        /// <returns>The original value if it is not null, otherwise <code>DBNull.Value</code>.</returns>
        public static object CheckValue(object value)
        {
            try
            {
                return value ?? DBNull.Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        /// <summary>
        /// Convert to DataTable to List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(DataTable dt)
        {
            List<T> targetList=null;
            try
            {
                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
                var columnNames = dt.Columns.Cast<DataColumn>()
                    .Select(c => c.ColumnName)
                    .ToList();
                var objectProperties = typeof(T).GetProperties(flags);
                targetList = dt.AsEnumerable().Select(dataRow =>
                {
                    var instanceOfT = Activator.CreateInstance<T>();

                    foreach (var properties in objectProperties.Where(properties => columnNames.Contains(properties.Name) && dataRow[properties.Name] != DBNull.Value))
                    {
                        properties.SetValue(instanceOfT, dataRow[properties.Name], null);
                    }
                    return instanceOfT;
                }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return targetList;
        }
        
        /// <summary>       
        /// Creates the mapping
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <returns></returns>
        private static Func<T1, T2> CreateMapping<T1, T2>() where T2 : new()
        {
            var typeOfSource = typeof(T1);
            var typeOfDestination = typeof(T2);

            // use reflection to get a list of the properties on the source and destination types
            var sourceProperties = typeOfSource.GetProperties();
            var destinationProperties = typeOfDestination.GetProperties();

            // join the source properties with the destination properties based on name
            var properties = from sourceProperty in sourceProperties
                             join destinationProperty in destinationProperties
                             on sourceProperty.Name.ToUpper() equals destinationProperty.Name.ToUpper()
                             select new { SourceProperty = sourceProperty, DestinationProperty = destinationProperty };


            return (x) =>
            {
                var y = new T2();

                foreach (var property in properties)
                {
                    var value = property.SourceProperty.GetValue(x, null);
                    property.DestinationProperty.SetValue(y, value, null);
                }

                return y;
            };
        }
        #endregion
    }
}
