﻿using System;
using Swunmath.Models;
using MySql.Data.MySqlClient;
using Swunmath.DAL;
using System.Collections.Generic;
using System.Reflection;
using log4net;

namespace Swunmath.Dal
{
    public class EBooksDal
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Get all Grades
        /// </summary>       
        /// <returns></returns>
        public List<Grades> GetGrades()
        {
            List<Grades> lstGrades = null;
            try
            {
               var query= "SELECT Id,Grade FROM Grades order by Id asc";
               var dsGrades=SqlHelper.ExecuteSqlQuery(query);

               if (dsGrades == null || dsGrades.Tables.Count == 0) return lstGrades;

               lstGrades = Utility.ToList<Grades>(dsGrades.Tables[0]);               
            }
            catch (MySqlException sqlEx)
            {
                Log.Error("GetGrades :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("GetGrades :", ex);
                throw;
            }
            return lstGrades;
        }
    }
   
}
