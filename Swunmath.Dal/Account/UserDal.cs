﻿using System;
using System.Reflection;
using Swunmath.Models;
using MySql.Data.MySqlClient;
using Swunmath.DAL;
using log4net;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;

namespace Swunmath.Dal
{
    public class UserDal
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Validate the login user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User ValidateUser(string email, string password)
        {
            User userInfo = null;
            try
            {
                var query = "SELECT Id as UserId ,Password,Email,District,FirstName,LastName,grade_level Grade FROM swun_math.users WHERE email='" + email + "'";
                var dsUser = SqlHelper.ExecuteSqlQuery(query);

                if (dsUser == null || dsUser.Tables.Count == 0) return userInfo;

                var lstUser = Utility.ToList<User>(dsUser.Tables[0]);

                if (lstUser != null && lstUser.Count > 0)
                {
                    //Validating the WordPress password
                    if (!Crypt.CheckPassword(password, lstUser[0].Password))
                    {
                        //if password is wrong then check if user passed Master password for teacher login
                        //string masterPassowrd = ConfigurationManager.AppSettings["MasterTeacherLoginPassword"];

                        using (var context = new SWUNMATHEntities())
                        {
                            string masterPassowrd = "";

                            var objMP = context.MasterTeacherPasswords.FirstOrDefault();

                            if (objMP == null)
                            {
                                return userInfo;
                            }

                            masterPassowrd = objMP.MasterPassword;

                            if (masterPassowrd != password)
                            {
                                return userInfo;
                            }
                        }
                    }

                    userInfo = lstUser[0];
                }
            }
            catch (MySqlException sqlEx)
            {
                Log.Error("ValidateUser :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("ValidateUser :", ex);
                throw;
            }
            Log.Info("ValidateUser : Valid Credentials");
            return userInfo;
        }
        /// <summary>
        /// Get user session if not expired
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        private UserSession GetUserSession(UserSession userSession)
        {
            UserSession uSession = new UserSession();

            try
            {
                var expiretime = ConfigurationManager.AppSettings["expiretime"];
                //var querySelect = "select access_token,expired from swun_math.sm_access where user_id=" + userSession.UserId + " and DATE_ADD(expired, INTERVAL "+ expiretime + " MINUTE)> Now() order by  expired desc limit 1";
                var querySelect = "select access_token,expired from swun_math.sm_access where user_id=" + userSession.UserId + " and expired > Now() order by  expired desc limit 1";
                var dsUserSession = SqlHelper.ExecuteSqlQuery(querySelect);

                if (dsUserSession == null || dsUserSession.Tables.Count == 0) return uSession;
                if (dsUserSession.Tables[0].Rows.Count == 0) return uSession;

                DataRow row = dsUserSession.Tables[0].Rows[0];
                uSession.UserId = userSession.UserId;
                uSession.expired = Convert.ToDateTime(row["expired"]);
                uSession.access_token = Convert.ToString(row["access_token"]);
            }
            catch (MySqlException sqlEx)
            {
                Log.Error("GetUserSession :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("GetUserSession :", ex);
                throw;
            }

            return uSession;
        }
        /// <summary>
        ///  Create of update user session. 
        ///  create new user session if epired else update existing session and return the session details
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        public UserSession SaveUserSession(UserSession userSession)
        {
            UserSession uSession = userSession;

            try
            {
                uSession = GetUserSession(userSession);

                if (string.IsNullOrEmpty(uSession.access_token))
                    uSession = CreateUserSession(userSession);
                else
                    UpdateUserSession(uSession);

            }
            catch (MySqlException sqlEx)
            {
                Log.Error("SaveUserSession :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("SaveUserSession :", ex);
                throw;
            }
            Log.Info("SaveUserSession : User session Saved");
            return uSession;
        }
        /// <summary>
        /// Create user session and return created session
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        private UserSession CreateUserSession(UserSession userSession)
        {
            UserSession uSession = new UserSession();

            try
            {
                var expiretime = ConfigurationManager.AppSettings["expiretime"];
                var query = "insert into swun_math.sm_access(user_id, access_token, expired) values(" + userSession.UserId + ", uuid(), DATE_ADD(NOW(), INTERVAL " + expiretime + " MINUTE))";
                var id = SqlHelper.ExecuteNonQuery(query);

                var querySelect = "select access_token,expired from swun_math.sm_access where id=" + id + "";
                var dsUserSession = SqlHelper.ExecuteSqlQuery(querySelect);

                if (dsUserSession == null || dsUserSession.Tables.Count == 0) return uSession;

                DataRow row = dsUserSession.Tables[0].Rows[0];
                uSession.UserId = userSession.UserId;
                uSession.Id = id;
                uSession.expired = Convert.ToDateTime(row["expired"]);
                uSession.access_token = Convert.ToString(row["access_token"]);

            }
            catch (MySqlException sqlEx)
            {
                Log.Error("CreateUserSession :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("CreateUserSession :", ex);
                throw;
            }
            Log.Info("CreateUserSession : User session created");
            return uSession;
        }
        /// <summary>
        /// Update user Session if not expired.
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        private long UpdateUserSession(UserSession userSession)
        {
            long id = 0;
            try
            {
                var expiretime = ConfigurationManager.AppSettings["expiretime"];
                var query = "update swun_math.sm_access set expired = DATE_ADD(NOW(), INTERVAL " + expiretime + " MINUTE) where access_token= '" + userSession.access_token + "'";
                id = SqlHelper.ExecuteNonQuery(query);


            }
            catch (MySqlException sqlEx)
            {
                Log.Error("UpdateUserSession :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("UpdateUserSession :", ex);
                throw;
            }
            Log.Info("UpdateUserSession :  User session Updated");
            return id;
        }
        /// <summary>
        /// Delete user session
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        public long DeleteUserSession(UserSession userSession)
        {
            long id = 0;
            try
            {
                var query = "delete from swun_math.sm_access where access_token= '" + userSession.access_token + "'";
                id = SqlHelper.ExecuteNonQuery(query);


            }
            catch (MySqlException sqlEx)
            {
                Log.Error("DeleteUserSession :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("DeleteUserSession :", ex);
                throw;
            }
            Log.Info("DeleteUserSession :  User session Deleted");
            return id;
        }

        public int GetClassroomUserTypeIdEmail(string email)
        {
            int userTypeId = 0;

            try
            {
                string _connString = ConfigurationManager.ConnectionStrings["ClassroomDBConnString"].ConnectionString;

                string _sqlCommand = "SELECT UserTypeID FROM users WHERE StatusID = 1 AND UserTypeID in (2,4,5) AND Email = @Email";

                using (SqlConnection con = new SqlConnection(_connString))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand(_sqlCommand, con);
                    com.Parameters.AddWithValue("@Email", email);

                    userTypeId = Convert.ToInt32(com.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetClassroomUserTypeIdEmail :", ex);
                throw ex;
            }

            return userTypeId;
        }


        /// <summary>
        /// Validate the login user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public System.Collections.Generic.List<User> FindTeacherByEmail(string email)
        {
            System.Collections.Generic.List<User> matchingusers = new System.Collections.Generic.List<User>();
            try
            {
                var query = "SELECT Id as UserId ,Password,Email,District,FirstName,LastName,grade_level Grade FROM swun_math.users WHERE email='" + email + "'";
                var dsUser = SqlHelper.ExecuteSqlQuery(query);

                if (dsUser == null || dsUser.Tables.Count == 0 || dsUser.Tables.Count > 1)
                {
                    //return userInfo;
                }
                else
                {
                    var lstUser1 = Utility.ToList<User>(dsUser.Tables[0]);
                    matchingusers = lstUser1;
                    //return userInfo;
                }


                var lstUser = Utility.ToList<User>(dsUser.Tables[0]);

                //if (lstUser != null && lstUser.Count > 0)
                //{
                //    //Validating the WordPress password
                //    if (!Crypt.CheckPassword(password, lstUser[0].Password))
                //    {
                //        //if password is wrong then check if user passed Master password for teacher login
                //        //string masterPassowrd = ConfigurationManager.AppSettings["MasterTeacherLoginPassword"];

                //        using (var context = new SWUNMATHEntities())
                //        {
                //            string masterPassowrd = "";

                //            var objMP = context.MasterTeacherPasswords.FirstOrDefault();

                //            if (objMP == null)
                //            {
                //                return userInfo;
                //            }

                //            masterPassowrd = objMP.MasterPassword;

                //            if (masterPassowrd != password)
                //            {
                //                return userInfo;
                //            }
                //        }
                //    }

                //    userInfo = lstUser[0];
                //}
            }
            catch (MySqlException sqlEx)
            {
                Log.Error("FindTeacherByEmail :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("FindTeacherByEmail :", ex);
                throw;
            }
            Log.Info("FindTeacherByEmail : found user");
            return matchingusers;
        }




        public bool FetchNoticeBoardMessageSeen(string email)
        {
            bool hasSeen = false;

            try
            {
                string _connString = ConfigurationManager.ConnectionStrings["ClassroomDBConnString"].ConnectionString;

                string _sqlCommand = "SELECT Isnull(NoticeBoardMessageSeen,0) FROM users WHERE StatusID = 1 AND UserTypeID in (2,4,5) AND Email = @Email";

                using (SqlConnection con = new SqlConnection(_connString))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand(_sqlCommand, con);
                    com.Parameters.AddWithValue("@Email", email);

                    hasSeen = Convert.ToBoolean(com.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                Log.Error("FetchNoticeBoardMessageSeen :", ex);
                throw ex;
            }

            return hasSeen;
        }


        public string FetchNoticeBoardMessages()
        {
            string MessagesHTML = "";

            try
            {
                string _connString = ConfigurationManager.ConnectionStrings["ClassroomDBConnString"].ConnectionString;

                string _sqlCommand = "SELECT Isnull(Title,'') as Title,Isnull(ContentHTML,'') as ContentHTML,FromDate,ToDate FROM dbo.NoticeBoardMessagesForTeacher WHERE StatusID = 1 and GETUTCDATE() between FromDateUTC and ToDateUTC order by FromDate Desc,ToDate Desc";

                using (SqlConnection con = new SqlConnection(_connString))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand(_sqlCommand, con);
                    //var dsUserSession = com.ExecuteReader();

                    SqlDataReader reader = com.ExecuteReader();
                    if (!reader.HasRows)
                    { return null; }
                    else
                    {
                        while (reader.HasRows)
                        {
                            int i = 0;
                            while (reader.Read())
                            {
                                if (MessagesHTML.Length > 0)
                                {
                                    //MessagesHTML = MessagesHTML + "<BR><hr>" + reader.GetString(0) + " - " + Convert.ToString(reader.GetDateTime(2).ToShortDateString()) + "<BR>" + reader.GetString(1);

                                    MessagesHTML = MessagesHTML + "<BR><hr>" + reader.GetString(0)  + "<BR>" + reader.GetString(1);
                                }
                                else
                                {
                                    MessagesHTML = reader.GetString(0) +  "<BR>" + reader.GetString(1);
                                    //MessagesHTML = reader.GetString(0) + " - " + Convert.ToString(reader.GetDateTime(2).ToShortDateString()) + "<BR>" + reader.GetString(1);
                                }
                                i++;
                            }
                            reader.NextResult();
                        }
                        if (!reader.IsClosed)
                        { reader.Close(); }
                    }


                }
            }
            catch (Exception ex)
            {
                Log.Error("GetClassroomUserTypeIdEmail :", ex);
                throw ex;
            }

            return MessagesHTML;
        }


        public bool UpdateNoticeBoardMessageSeen(string email)
        {
            bool hasupdated = false;

            try
            {
                string _connString = ConfigurationManager.ConnectionStrings["ClassroomDBConnString"].ConnectionString;

                string _sqlCommand = "Update users set NoticeBoardMessageSeen = 1 WHERE StatusID = 1 AND UserTypeID in (2,4,5) AND Email = '"+ email+"'";

                using (SqlConnection con = new SqlConnection(_connString))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand(_sqlCommand, con);
                    com.ExecuteNonQuery();
                    hasupdated = true;
                }
            }
            catch (Exception ex)
            {
                hasupdated = false;
                Log.Error("FetchNoticeBoardMessageSeen :", ex);
                throw ex;
            }

            return hasupdated;
        }

    }

}
