﻿using System;
using System.Reflection;
using Swunmath.Models;
using MySql.Data.MySqlClient;
using Swunmath.DAL;
using log4net;

namespace Swunmath.Dal
{
    public class MenuHeaderDal
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Get Menu Header
        /// </summary>
        /// <returns></returns>
        public MenuHeader GetMenus()
        {
            try
            {
                //Replace # with =0(to get Main menu) or in(745,746,788,755,1427,1380) => to get sub menus in sqlQuery
                //var menuQuery = "SELECT p.ID Id, p.post_title PostTitle, p.post_name PostName, p.menu_order MenuOrder, n.post_name as Name, n.post_title as Title, m.meta_value as Url, pp.meta_value as MenuParent FROM swun.sm_term_relationships as txr INNER JOIN swun.sm_posts as p ON txr.object_id = p.ID LEFT JOIN swun.sm_postmeta as m ON p.ID = m.post_id LEFT JOIN swun.sm_postmeta as pl ON p.ID = pl.post_id AND pl.meta_key = '_menu_item_object_id' LEFT JOIN swun.sm_postmeta as pp ON p.ID = pp.post_id AND pp.meta_key = '_menu_item_menu_item_parent' LEFT JOIN swun.sm_posts as n ON pl.meta_value = n.ID WHERE txr.term_taxonomy_id = 131 AND p.post_status='publish' AND p.post_type = 'nav_menu_item' AND m.meta_key = '_menu_item_url' and pp.meta_value # ORDER BY p.menu_order;";
                //return SqlHelper.GetMenus(menuQuery);

                string WPMainDBName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WPMainDBName"]);
                //Replace # with =0(to get Main menu) or in(745,746,788,755,1427,1380) => to get sub menus in sqlQuery
                var menuQuery = "SELECT p.ID Id, p.post_title PostTitle, p.post_name PostName, p.menu_order MenuOrder, n.post_name as Name, n.post_title as Title, m.meta_value as Url, pp.meta_value as MenuParent FROM " + WPMainDBName + ".sm_term_relationships as txr INNER JOIN " + WPMainDBName + ".sm_posts as p ON txr.object_id = p.ID LEFT JOIN " + WPMainDBName + ".sm_postmeta as m ON p.ID = m.post_id LEFT JOIN " + WPMainDBName + ".sm_postmeta as pl ON p.ID = pl.post_id AND pl.meta_key = '_menu_item_object_id' LEFT JOIN " + WPMainDBName + ".sm_postmeta as pp ON p.ID = pp.post_id AND pp.meta_key = '_menu_item_menu_item_parent' LEFT JOIN " + WPMainDBName + ".sm_posts as n ON pl.meta_value = n.ID WHERE txr.term_taxonomy_id = 131 AND p.post_status='publish' AND p.post_type = 'nav_menu_item' AND m.meta_key = '_menu_item_url' and pp.meta_value # ORDER BY p.menu_order;";
                return SqlHelper.GetMenus(menuQuery);


                
            }
            catch (MySqlException sqlEx)
            {
                Log.Error("MenuHeader :", sqlEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("MenuHeader :", ex);
                throw;
            }
        }
    }
   
}
