﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Swunmath.Models.Common;

namespace Swunmath.Service
{
    public class MethodsApiController : ApiController
    {
        [HttpGet]
        public IEnumerable<HelpMethod> GetAllMethods()
        {
            // Get the IApiExplorer registered automatically
            var ex = this.Configuration.Services.GetApiExplorer();

            // loop, convert and return all descriptions 
            return ex.ApiDescriptions
                // ignore self
                .Where(d => d.ActionDescriptor.ControllerDescriptor.ControllerName != "ApiMethod")
                .Select(d =>
                {
                    // convert to a serializable structure
                    return new HelpMethod
                    {
                        Parameters = d.ParameterDescriptions.Select(p => new HelpParameter
                        {
                            Name = p.Name,
                            Type = p.ParameterDescriptor.ParameterType.FullName,
                            IsOptional = p.ParameterDescriptor.IsOptional
                        }).ToArray(),
                        Method = d.HttpMethod.ToString(),
                        RelativePath = d.RelativePath,
                        ReturnType = d.ResponseDescription.DeclaredType == null ?
                            null : d.ResponseDescription.DeclaredType.ToString()
                    };
                });
        }
    }

}
