﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Swunmath.Models;
using Swunmath.Dal;

namespace Swunmath.Service
{
    [RoutePrefix("Resource")]
    public class ResourceApiController : ApiController
    {
        readonly EBooksDal _eBooksDal = new EBooksDal();
        // Get Resource/Grades
        [HttpGet]
        [Route("Grades")]
        public HttpResponseMessage GetGrades()
        {
            try
            {
                var lstGrades = _eBooksDal.GetGrades();
                var message = Request.CreateResponse(HttpStatusCode.Created, lstGrades);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
