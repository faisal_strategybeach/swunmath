﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Swunmath.Models;
using Swunmath.Dal;

namespace Swunmath.Service
{
    [RoutePrefix("Account")]
    public class AccountApiController : ApiController
    {
        readonly UserDal _userDal = new UserDal();
        // POST Account/ValidateUser
        [HttpPost]
        [Route("ValidateUser")]
        public HttpResponseMessage ValidateUser(User user)
        {
            try
            {
                user = _userDal.ValidateUser(user.Email, user.Password);
                var message = Request.CreateResponse(HttpStatusCode.Created, user);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        /// <summary>
        /// Save user session in data base and return guid
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveUserSession")]
        public HttpResponseMessage SaveUserSession(UserSession  userSession)
        {
            try
            {
               var  user = _userDal.SaveUserSession(userSession);
                var message = Request.CreateResponse(HttpStatusCode.Created, user);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        /// <summary>
        /// delete session from database
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteUserSession")]
        public HttpResponseMessage DeleteUserSession(UserSession userSession)
        {
            try
            {
                var user = _userDal.DeleteUserSession(userSession);
                var message = Request.CreateResponse(HttpStatusCode.Created, user);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("FindTeacherByEmail")]
        public HttpResponseMessage FindTeacherByEmail(User user)
        {
            try
            {
                string email = user.Email;
                System.Collections.Generic.List<User> matchinguser = _userDal.FindTeacherByEmail(email);
                var message = Request.CreateResponse(HttpStatusCode.Created, matchinguser);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }



        [HttpPost,HttpGet]
        [Route("FetchNoticeBoardMessageSeen")]
        public HttpResponseMessage FetchNoticeBoardMessageSeen(User user)
        {
            bool hasSeen = false;
            try
            {
                hasSeen = _userDal.FetchNoticeBoardMessageSeen(user.Email);
                var message = Request.CreateResponse(HttpStatusCode.Created, hasSeen);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [HttpPost, HttpGet]
        [Route("FetchNoticeBoardMessages")]
        public HttpResponseMessage FetchNoticeBoardMessages(User user)
        {
            string MessagesHTML = "";
            try
            {
                MessagesHTML = _userDal.FetchNoticeBoardMessages();
                var message = Request.CreateResponse(HttpStatusCode.Created, MessagesHTML);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("UpdateNoticeBoardMessageSeen")]
        public HttpResponseMessage UpdateNoticeBoardMessageSeen(User user)
        {
            bool hasUpdated = false;
            try
            {
                hasUpdated = _userDal.UpdateNoticeBoardMessageSeen(user.Email);
                var message = Request.CreateResponse(HttpStatusCode.Created, hasUpdated);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
