﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Swunmath.Dal;

namespace Swunmath.Service
{
    [RoutePrefix("MenuHeader")]
    public class MenuHeaderApiController : ApiController
    {
        readonly MenuHeaderDal _menuHeaderDal = new MenuHeaderDal();
        // Get MenuHeaderApi/GetMenus
        [HttpGet]
        [Route("GetMenus")]
        public HttpResponseMessage GetMenus()
        {
            try
            {
                var lstMenus = _menuHeaderDal.GetMenus();
                var message = Request.CreateResponse(HttpStatusCode.Created, lstMenus);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
