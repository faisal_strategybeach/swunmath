﻿using SwunmathWeb.Helper;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SwunmathWeb
{

    public class Authentication : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var ctx = default(HttpContext);
            ctx = HttpContext.Current;

            base.OnActionExecuting(context);

            LogToFile.Log("Authentication/OnActionExecuting", "Action Start", "URL=", Convert.ToString(ctx.Request.Url),
                "RefURL=", Convert.ToString(ctx.Request.UrlReferrer));


            if (!ctx.User.Identity.IsAuthenticated)
            {
                context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "TeacherLogin" }));
                LogToFile.Log("Authentication/OnActionExecuting", "Not Authenticated, Redirect to Account/TeacherLogin", "URL=", Convert.ToString(ctx.Request.Url),
                "RefURL=", Convert.ToString(ctx.Request.UrlReferrer));
            }

            string cookieName = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieName"]) ? "teacheridentifier" : ConfigurationManager.AppSettings["SwunmathCookieName"];
            string cookieDomain = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieDomain"]) ? "swunmath.com" : ConfigurationManager.AppSettings["SwunmathCookieDomain"];
            string cookieExpiresMinutes = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieExpires"]) ? "60" : ConfigurationManager.AppSettings["SwunmathCookieExpires"];

            if (ctx.Request.Cookies[cookieName] != null)
            {
                LogToFile.Log("Authentication/OnActionExecuting", "Coolke Found", "URL=", Convert.ToString(ctx.Request.Url),
                "RefURL=", Convert.ToString(ctx.Request.UrlReferrer));

                HttpCookie cookie = ctx.Request.Cookies[cookieName];
                cookie.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(cookieExpiresMinutes));
                cookie.Domain = cookieDomain;
                cookie.Secure = true;

                //ctx.Response.Cookies.Add(cookie);
                ctx.Response.Cookies.Set(cookie);
            }
            else
            {
                var newCookieName = cookieName + "_new";

                if (ctx.Request.Cookies[newCookieName] != null)
                {

                }

                LogToFile.Log("Authentication/OnActionExecuting", "Coolke Not Found", "URL=", Convert.ToString(ctx.Request.Url), "RefURL=", Convert.ToString(ctx.Request.UrlReferrer));

                if (ctx.Request.Url.Host != "localhost")
                {
                    //context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "Logout" })); 
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "TeacherLogin" }));
                }

                LogToFile.Log("Authentication/OnActionExecuting", "Redirect to Account/Logout", "URL=", Convert.ToString(ctx.Request.Url),
                "RefURL=", Convert.ToString(ctx.Request.UrlReferrer));
            }
        }

    }

}