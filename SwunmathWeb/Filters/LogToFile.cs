﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SwunmathWeb.Filters
{
    public class LogToFile
    {
        public static void Log(params string[] LogStrings)
        {

            // LogToFile.Log("userForOAuthVerificationResult", JsonConvert.SerializeObject(userForOAuthVerificationResult)); Exam ple Call
            string LogFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FileLogFolder"]);
            string FileLogYN = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FileLogYN"]);
            if (LogFolder != null && (FileLogYN.Trim().ToUpper() == "Y" || FileLogYN.Trim().ToUpper() == "YES"))
            {

                try
                {


                    if (!Directory.Exists(LogFolder))
                    {
                        Directory.CreateDirectory(LogFolder);
                    }

                    string logFile = DateTime.Now.ToString("ddMMyyyy") + ".txt";
                    string logFilefullpath = LogFolder + "\\" + logFile;

                    if (!File.Exists(logFilefullpath))
                    {
                        FileStream f = File.Create(logFilefullpath);
                        f.Close();
                    }

                    using (StreamWriter sw = new StreamWriter(logFilefullpath, true))
                    {
                        sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF"));
                        sw.WriteLine(string.Join(" - ", LogStrings));
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {

                }


            }
        }

    }
}