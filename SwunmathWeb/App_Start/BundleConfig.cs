﻿using System.Web;
using System.Web.Optimization;

namespace SwunmathWeb
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                      "~/Content/css/style.css",
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/font-awesome.css",
                      "~/Content/css/minimal.min.css"));
            bundles.Add(new StyleBundle("~/bundles/wpStyles").Include(
                "~/Content/css/wp_content_style.css",
                "~/Content/css/custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/Scripts/jquery-3.3.1.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryVal").Include(
                "~/Content/Scripts/jquery.validate.js").Include(
                "~/Content/Scripts/jquery.validate.unobtrusive.js"));
            bundles.Add(new ScriptBundle("~/bundles/menuheader").Include(
                "~/Content/Scripts/menuheader.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap/Js").Include(
                "~/Content/Scripts/bootstrap.js"));
            BundleTable.EnableOptimizations = false;
        }
    }
}
