﻿using System.ComponentModel.DataAnnotations;
using SwunmathWeb.TranslationManager;

namespace SwunmathWeb.Models
{public class User
    {
        public int UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "InvalidEmail")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "Password")]
        public string Password { get; set; }
        public string StoredPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? District { get; set; }
        public string Grade { get; set; }
    }
}