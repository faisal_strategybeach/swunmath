﻿using SwunmathWeb.TranslationManager;

namespace SwunmathWeb.Helper
{
    public static class ResourceManager
    {
        public static string GetResourceErrorMsg(string resourceKey)
        {
            return ErrorMessage.ResourceManager.GetString(resourceKey);
        }
        public static string GetResourceMsg(string resourceKey)
        {
            return CommonMessage.ResourceManager.GetString(resourceKey);
        }
    }
}