﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Swunmath.Models;
using SwunmathWeb.Services;

namespace SwunmathWeb
{
    public static class Common
    {
        public static List<string> GetlstGradeLevelA()
        {
            var emailGradeA = Convert.ToString(ConfigurationManager.AppSettings["GradeAEmail"]);
            var district = Convert.ToString(ConfigurationManager.AppSettings["District"]);
            var grades = Constants.GetConstantGrades();
            var gradeLevel = Convert.ToString(HttpContext.Current.Session["Grade"]);

            if (gradeLevel == "0") {
                gradeLevel = "A";
            }
            
            if (gradeLevel == "A")
            {
                if (Convert.ToString(HttpContext.Current.Session["District"]) == district)
                {
                    return grades["district"];
                }
                else if (Convert.ToString(HttpContext.Current.Session["Email"]) == emailGradeA)
                {
                    return grades[emailGradeA];
                }
                else
                {
                    return grades["all"];
                }
            }
            else
            {
                return grades[gradeLevel];
            }
        }

        public static MenuHeader GetMenus()
        {
            var response = GetAsync("MenuHeader/GetMenus");
            var menus = JsonConvert.DeserializeObject<MenuHeader>(response.Result);
            return menus;
        }

        public static string ConvertStringToBase64String(string normalString)
        {
            if (string.IsNullOrEmpty(normalString))
            {
                return normalString;
            }

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(normalString);
            return System.Convert.ToBase64String(data);
        }

        public static string ConvertBase64StringToString(string base64EncodedString)
        {
            if (string.IsNullOrEmpty(base64EncodedString))
            {
                return base64EncodedString;
            }

            byte[] data = System.Convert.FromBase64String(base64EncodedString);
            return System.Text.ASCIIEncoding.ASCII.GetString(data);
        }

        #region Private Methods
        /// <summary>
        /// Get Web Api call
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static async Task<string> GetAsync(string url)
        {
            var message = await ServiceRepository.GetAsync(url).ConfigureAwait(false);
            return message;
        }

        #endregion
    }
}