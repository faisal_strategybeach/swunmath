﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace SwunmathWeb
{
    public static class Constants
    {
        public static readonly List<string> SbcGradesList= new List<string>(){ "TK", "K", "1", "2"};
        public static Dictionary<string, List<string>> GetConstantGrades()
        {
            var emailGradeA = Convert.ToString(ConfigurationManager.AppSettings["GradeAEmail"]);
            var district = Convert.ToString(ConfigurationManager.AppSettings["District"]);
            var grades =new Dictionary<string, List<string>>();
            grades.Add("district", new List<string>() { "K", "1", "2", "3", "4","5", "6" });
            grades.Add(emailGradeA, new List<string>() { "TK", "K", "1", "2", "3", "4", "5", "6" });
            grades.Add("all", new List<string>() { "TK", "K", "1", "2", "3", "4", "5", "6","7","8" });
            grades.Add("TK", new List<string>() { "TK", "K"});
            grades.Add("K", new List<string>() { "TK", "K","1" });
            grades.Add("1", new List<string>() { "K", "1", "2" });
            grades.Add("2", new List<string>() { "1", "2", "3" });
            grades.Add("3", new List<string>() { "2", "3", "4" });
            grades.Add("4", new List<string>() { "3", "4", "5" });
            grades.Add("5", new List<string>() { "4", "5", "6" });
            grades.Add("6", new List<string>() { "5", "6", "7" });
            grades.Add("7", new List<string>() { "6", "7", "8" });
            grades.Add("8", new List<string>() { "7", "8"});
            return grades;
        }
    }
}