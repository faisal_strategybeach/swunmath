﻿//dragElement(document.getElementById("note1"));

var _notes;
var _icon_top;

function setNotes(notes) {
    _notes = notes;
}

setInterval(function () {
    updatePositions();
}, 1000);

function updatePositions() {
    if (_notes) {
        _icon_top = 23;
        for (var i = 0; i < _notes.length; i++) {
            var o = _notes[i];
            updateNoteIconPostionInUI(document.getElementById("note" + o.nid), o)
        }
    }
}

function updateNoteIconPostionInUI(noteElem, note) {
    //if (note.iconPos != null && noteElem != null) {
    if (noteElem != null) {
        if (note.iconPos != null) {
            var pos = JSON.parse(note.iconPos);
            noteElem.style["top"] = pos.top;
            noteElem.style["left"] = pos.left;
            noteElem.style["width"] = pos.width ? pos.width : '30px';
        }
        else {
            noteElem.style["top"] = _icon_top + 'px'; //'23px';
            noteElem.style["left"] = '0px';
            noteElem.style["width"] = '30px';

            _icon_top = _icon_top + 35;
        }
    }
}

function dragElement(elmnt, note) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";

        updateIconPositionOfObject(elmnt.style.top, elmnt.style.left);
    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;

        console.log(rootUrl);
        console.log(note);
        console.log('element style, top: ' + elmnt.style["top"] + ' || left: ' + elmnt.style["left"]);

        saveNoteIconPostionInDB();
    }

    function updateIconPositionOfObject(top, left) {

        if (note.NoteID == undefined) {
            note.NoteID = getNoteId();
        }

        console.log(_notes);
        for (var i = 0; i < _notes.length; i++) {
            if (note.NoteID == _notes[i].NoteID) {
                if (_notes[i].iconPos) {
                    var jsonPos = JSON.parse(_notes[i].iconPos);
                    jsonPos.top = top;
                    jsonPos.left = left;

                    _notes[i].iconPos = JSON.stringify(jsonPos);
                }
                else {
                    _notes[i].iconPos = JSON.stringify({
                        top: top,
                        left: left
                    });
                }

                note = _notes[i];
            }
        }
    }

    function saveNoteIconPostionInDB() {
        var data = JSON.stringify({
            top: elmnt.style["top"],
            left: elmnt.style["left"],
            width: elmnt.style["width"],
            transform: elmnt.style["transform"]
        });

        data = btoa(data);

        const xhr = new XMLHttpRequest();

        xhr.addEventListener('readystatechange', function () {
            if (this.readyState === this.DONE) {
                console.log(this.responseText)
            }
        })

        var noteId = getNoteId();

        xhr.open('PUT', rootUrl + 'Notes/UpdateNoteIconPosition/' + noteId + '/' + data);
        //xhr.open('PUT', rootUrl + 'Notes/UpdateNoteIconPosition/' + note.NoteID);
        xhr.setRequestHeader('content-type', 'application/json');

        xhr.send();
    }

    function getNoteId() {
        if (note.NoteID != undefined) {
            return note.NoteID;
        }
        else {
            for (var i = 0; i < _notes.length; i++) {
                if (note.nid == _notes[i].nid && note.note == _notes[i].note) {
                    return _notes[i].NoteID;
                }
            }
        }
    }
}