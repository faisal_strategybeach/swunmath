window.onload = function (e) {
    CheckIfUserLoggedInforEBook();
};

function CheckIfUserLoggedInforEBook() {
    var request = new XMLHttpRequest()

    var apiUrl = window.location.origin + '/Account/ValidateUserForBooksAccess';

    request.open('GET', apiUrl, true);
    request.onload = function () {
        // Begin accessing JSON data here
        //var data = JSON.parse(this.response);
		var data = this.response;

        if (request.status >= 200 && request.status < 400) {
            //user not logged in
            if (data.toLowerCase() == "false") {
                window.location.href = window.location.origin;
            }
        } else {
            console.log('error');
            console.log(data);
        }
    }

    request.send()
}