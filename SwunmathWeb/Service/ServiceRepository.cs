﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;

namespace SwunmathWeb.Services
{
    //This class contains common functions/methods that can be re-used in the application
    public class ServiceRepository
    {
        public static string BaseUrl = ConfigurationManager.AppSettings["BaseUrl"].ToString();
        
        //Common method used to Get data from an API
        public static async Task<string> GetAsync(string requestUri)
        {
            var response = string.Empty;
            if (!string.IsNullOrEmpty(BaseUrl) && !string.IsNullOrEmpty(requestUri))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    var result = await client.GetAsync(requestUri).ConfigureAwait(false);  //for eg. requestUri="api/GetAll"
                    if (result.IsSuccessStatusCode)
                    {
                        response = result.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            return response;
        }

        //Method to check whether a URL exists or not
        public static async Task<bool> UrlExists(string requestUri)
        {
            var response = string.Empty;
            var isExists = false;
            if (!string.IsNullOrEmpty(BaseUrl) && !string.IsNullOrEmpty(requestUri))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(requestUri);
                    var result = await client.GetAsync(requestUri).ConfigureAwait(false);  //for eg. requestUri="api/GetAll"
                    isExists = result.IsSuccessStatusCode;
                }
            }
            return isExists;
        }

        //Common method used to Post data to an API
        public static async Task<string> PostAsync<T>(string requestUri,T type)
        {
            var response = string.Empty;
            if (!string.IsNullOrEmpty(BaseUrl) && !string.IsNullOrEmpty(requestUri))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);

                    var myContent = JsonConvert.SerializeObject(type);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    var result = await client.PostAsync(requestUri, byteContent).ConfigureAwait(false);
                    if (result.IsSuccessStatusCode)
                    {
                        response = result.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            return response;
        }

        /// <summary>
        /// Checks the Remote file exists or not.
        /// </summary>
        /// <param name="url">The URL of the remote file.</param>
        /// <returns>True : If the file exits, False if file not exists</returns>
        private bool RemoteFileExists(string url)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                if (response != null)
                {
                    response.Close();
                }
                //Returns TRUE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }
    }
}