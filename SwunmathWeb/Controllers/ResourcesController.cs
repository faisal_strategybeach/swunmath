﻿using System;
using SwunmathWeb.Services;
using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;
using Swunmath.Models;
using System.Collections.Generic;
using Swunmath.Dal;
using System.Linq;
using SwunmathWeb.Helper;

namespace SwunmathWeb
{
    public class ResourcesController : Controller
    {
        [Authentication]
        public ActionResult EBooksList()
        {
            try
            {
                LogToFile.Log("Resources/EBooksList", "Action Start", "URL=", Convert.ToString(Request.Url), "RefURL=", "URL=", Convert.ToString(Request.UrlReferrer));

                var response = GetAsync("Resource/Grades");
                var lstGrade = JsonConvert.DeserializeObject<List<Grades>>(response.Result);
                ViewBag.Grades = lstGrade;
                ViewBag.guid = Session["guid"];

                User userobj = new User();
                string UserEmail = Convert.ToString(Session["Email"]);
                userobj.Email = UserEmail;
                var responseMessages = PostAsync("Account/FetchNoticeBoardMessages", userobj);
                string AllMessages = JsonConvert.DeserializeObject<string>(responseMessages.Result);


                var responseMessageseen = PostAsync("Account/FetchNoticeBoardMessageSeen", userobj);
                bool messageSeen = JsonConvert.DeserializeObject<bool>(responseMessageseen.Result);



                //ViewBag.NoticeMessageHTML = @"Some details hereSome details hereSome details hereSome details hereSome details hereSome details hereSome details hereSome details hereSome details hereSome details here<br />
                //Some details here<br />
                //Some details here<br />
                //Some details here<br />
                //Some details here<br />
                //Some details here<br />
                //Some details here<br />
                //Some details here<br />
                //Some details here<br />";

                ViewBag.NoticeMessageHTML = AllMessages;

                ViewBag.NoticeMessageDisplayed = messageSeen;// true;

                try
                {
                    using (var dbContext = new SWUNMATHEntities())
                    {
                        ViewBag.eBookConfigs = dbContext.eBooks.Where(w => w.BookType == "T").ToList();
                    }
                }
                catch (Exception ex)
                {
                    LogToFile.Log("Resources/EBooksList", "Inner try catch", "Error=" + ex.Message
                        , "URL=", Convert.ToString(Request.Url), "RefURL=", "URL=", Convert.ToString(Request.UrlReferrer));
                }

                LogToFile.Log("Resources/EBooksList", "Action End");

                return View();
            }
            catch (Exception ex)
            {
                LogToFile.Log("Resources/EBooksList", "Outer try catch", "Error=" + ex.Message
                    , "URL=", Convert.ToString(Request.Url), "RefURL=", "URL=", Convert.ToString(Request.UrlReferrer));

                return View("_Error");
            }
        }

        [HttpGet]
        public string GetConfigData(string gradePdfName)
        {
            try
            {
                using (var dbContext = new SWUNMATHEntities())
                {
                    var eBook = dbContext.eBooks.Where(X => (X.PdfPath.ToLower() == gradePdfName.ToLower())).FirstOrDefault();
                    if (eBook != null)
                        return eBook.ConfigString;
                }
            }
            catch (Exception) { }

            return "";
        }

        #region Private Methods
        /// <summary>
        /// Get Web Api call
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static async Task<string> GetAsync(string url)
        {
            var message = await ServiceRepository.GetAsync(url).ConfigureAwait(false);
            return message;
        }

        private static async Task<string> PostAsync<T>(string url, T type)
        {
            var message = await ServiceRepository.PostAsync<T>(url, type).ConfigureAwait(false);
            return message;
        }

        #endregion

        [Authentication]
        public ActionResult ProfessionalDevelopment()
        {
            return View();
        }

        //[Authentication]
        public ActionResult SummerSchool(string pagePassword)
        {
            // Not using session for now, will check with client and confirm
            string pagePasswordValue = "";
            if (pagePassword==null || pagePassword == "")
            {
                //pagePasswordValue = Convert.ToString(Session["SummerSchoolPagePassword"]);
            }
            else
            {
                string decryptedpagePassword = "";
                try
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(pagePassword);
                    decryptedpagePassword = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                }
                catch (Exception ex)
                {
                    
                }

                //Session["SummerSchoolPagePassword"] = decryptedpagePassword; 
                ViewBag.providedPwd = decryptedpagePassword;
            }
            return View();
        }


        public bool FetchNoticeBoardMessages()
        {
            User userobj = new User();
            string UserEmail = Convert.ToString(Session["Email"]);
            userobj.Email = UserEmail;
            var responseMessages = PostAsync("Account/UpdateNoticeBoardMessageSeen", userobj);
            bool AllMessages = Convert.ToBoolean(responseMessages.Result);


            return AllMessages;
        }


    }
}