﻿using System;
using System.Reflection;
using System.Web;
using System.Threading.Tasks;
using System.Web.Mvc;
using SwunmathWeb.Services;
using System.Web.Security;
using log4net;
using Newtonsoft.Json;
using SwunmathWeb.Models;
using SwunmathWeb.Helper;
using System.Configuration;
using Swunmath.Dal;
using System.Web.Routing;

namespace SwunmathWeb
{
    public class AccountController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Show the Teacher Login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TeacherLogin()
        {
            LogToFile.Log("Account/Teacher Login", "Action Start", "URL=", Convert.ToString(Request.Url), "RefURL=", "URL=", Convert.ToString(Request.UrlReferrer));


            string cookieName = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieName"]) ? "teacheridentifier" : ConfigurationManager.AppSettings["SwunmathCookieName"];
            string cookieDomain = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieDomain"]) ? "swunmath.com" : ConfigurationManager.AppSettings["SwunmathCookieDomain"];
            string cookieExpiresMinutes = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieExpires"]) ? "60" : ConfigurationManager.AppSettings["SwunmathCookieExpires"];

            


            if (Session["guid"] == null || Request.Cookies[cookieName]==null)
            {
                LogToFile.Log("Account/Teacher Login", "Session not found, View login page", "URL=", Convert.ToString(Request.Url), "RefURL=", "URL=", Convert.ToString(Request.UrlReferrer));

                FormsAuthentication.SignOut();
                Session.Abandon();
                return View();
            }
            LogToFile.Log("Account/Teacher Login", "Session found, Redirect to Resources/EBooksList", "URL=", Convert.ToString(Request.Url), "RefURL=", "URL=", Convert.ToString(Request.UrlReferrer));

            return RedirectToAction("EBooksList", "Resources");
        }


        [HttpGet]
        public bool ValidateUserForBooksAccess()
        {
            //bool result = false;
            if (Session["Email"] != null)
                return true;

            return false;
            //return Json(new { isUserLoggedIn = result }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Validate the login user
        /// 1. If the user is valid then redirect to eBooks list page
        /// 2. Invalid User then redirect to Teacher Login Page
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TeacherLogin(User userModel)
        {
            try
            {
                //Pending Decrypt the password
                if (!ModelState.IsValid)
                {
                    ViewBag.ErrorMsg = ResourceManager.GetResourceMsg("EmptyCredentials");
                    return View("TeacherLogin");
                }
                var response = PostAsync("Account/ValidateUser", userModel);
                var userInfo = JsonConvert.DeserializeObject<User>(response.Result);
                if (userInfo == null)
                {
                    ViewBag.ErrorMsg = ResourceManager.GetResourceErrorMsg("InvalidCredentials");
                    return View("TeacherLogin");
                }
                //Log user session in database
                UserSession userSession = new UserSession();
                userSession.UserId = userInfo.UserId;
                var sessionResponse = PostAsync("Account/SaveUserSession", userSession);
                var guid = JsonConvert.DeserializeObject<UserSession>(sessionResponse.Result);


                //If the User is valid Set Authentication and redirect to the eBooks list page
                FormsAuthentication.SetAuthCookie(userInfo.Email, false);
                System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie(
                    FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(new FormsAuthenticationTicket(1, userInfo.Email, DateTime.Now,
                        DateTime.Now.AddMinutes(20160), false, ""))));
                Session["District"] = userInfo.District;
                Session["Grade"] = userInfo.Grade;
                Session["Email"] = userInfo.Email;
                Session["guid"] = guid.access_token;
                Session["UserID"] = userInfo.UserId;
                Session["UserEmail"] = userInfo.Email;

                //set cookie for other domain.                
                string cookieSeparator = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieSeparator"]) ? "||" : ConfigurationManager.AppSettings["SwunmathCookieSeparator"];
                string cookieName = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieName"]) ? "teacheridentifier" : ConfigurationManager.AppSettings["SwunmathCookieName"];
                string cookieDomain = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieDomain"]) ? "swunmath.com" : ConfigurationManager.AppSettings["SwunmathCookieDomain"];
                string cookieExpiresMinutes = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieExpires"]) ? "60" : ConfigurationManager.AppSettings["SwunmathCookieExpires"];

                string cookieValue = userInfo.Email + cookieSeparator + guid.access_token; // vasim@strategybeach.com||d49f9d07-d095-11ea-8554-f23c91181be8
                string base64EncodedCookieValue = Common.ConvertStringToBase64String(cookieValue);

                HttpCookie httpCookie = new HttpCookie(cookieName);
                httpCookie.Value = base64EncodedCookieValue; // "hello";
                httpCookie.Domain = cookieDomain; //"microsoft.com";
                httpCookie.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(cookieExpiresMinutes));
                httpCookie.Secure = true;

                Response.Cookies.Add(httpCookie);
                Response.Write(Request.Cookies[cookieName].Value);
                //end

                #region Test Cookie
                //set cookie for other domain.                

                var newCookieName = cookieName + "_new";

                HttpCookie httpCookieNew = new HttpCookie(newCookieName);
                httpCookieNew.Value = base64EncodedCookieValue; // "hello";
                httpCookieNew.Domain = cookieDomain; //"microsoft.com";
                httpCookieNew.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(cookieExpiresMinutes));
                //httpCookieNew.Secure = true;

                Response.Cookies.Add(httpCookieNew);
                Response.Write(Request.Cookies[newCookieName].Value);
                //end
                #endregion

                Session["token"] = base64EncodedCookieValue;

                Log.Info("Valid Credentials");

                UserDal _user = new UserDal();

                int userTypeId = _user.GetClassroomUserTypeIdEmail(userInfo.Email);

                Log.Info("UserTypeId = " + userTypeId);

                if (userTypeId == 4)
                {
                    return Redirect(ConfigurationManager.AppSettings["ClassroomDistrictAdminRedirectURL"]);
                }
                else if (userTypeId == 5)
                {
                    return Redirect(ConfigurationManager.AppSettings["ClassroomSchoolAdminRedirectURL"]);
                }
                else
                {
                    LogToFile.Log("Account/TeacherLogin[Post]", "Redirect to Resources/EBooksList", "URL=", Convert.ToString(Request.Url),
                        "RefURL=", Convert.ToString(Request.UrlReferrer));

                    if (Request.Url.Host != "localhost")
                    {
                        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                        return Redirect(u.Action("EBooksList", "Resources", null, "https"));
                    }
                    else
                    {
                        return RedirectToAction("EBooksList", "Resources");
                    }
                }

            }
            catch (Exception)
            {
                return View("_Error");
            }
        }

        [Authorize]
        public RedirectResult Logout()
        {
            LogToFile.Log("Account/Logout", "Action Start", "URL=", Convert.ToString(Request.Url), "RefURL=", Convert.ToString(Request.UrlReferrer));

            UserSession userSession = new UserSession();
            userSession.access_token = Convert.ToString(Session["guid"]);

            if (!string.IsNullOrEmpty(userSession.access_token))
            {
                var sessionResponse = PostAsync("Account/DeleteUserSession", userSession);
                var id = JsonConvert.DeserializeObject<string>(sessionResponse.Result);
            }

            string cookieName = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieName"]) ? "teacheridentifier" : ConfigurationManager.AppSettings["SwunmathCookieName"];
            string cookieDomain = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieDomain"]) ? "swunmath.com" : ConfigurationManager.AppSettings["SwunmathCookieDomain"];

            if (Request.Cookies[cookieName] != null)
            {
                LogToFile.Log("Account/Logout", "Cookie Found Deleting cookie", "URL=", Convert.ToString(Request.Url), "RefURL=", Convert.ToString(Request.UrlReferrer));

                HttpCookie cookie = Request.Cookies[cookieName];
                cookie.Expires = DateTime.Now.AddYears(-1);
                cookie.Domain = cookieDomain;
                cookie.Secure = true;

                Response.Cookies.Add(cookie);
                Request.Cookies.Remove(cookieName);
            }

            FormsAuthentication.SignOut();
            Session.Abandon();

            LogToFile.Log("Account/Logout", "Action End, redirecting to Account/TeacherLogin", "URL=", Convert.ToString(Request.Url), "RefURL=", Convert.ToString(Request.UrlReferrer));

            return this.Redirect("TeacherLogin");
        }

        #region PrivateMethods
        /// <summary>
        /// Post Web Api call
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static async Task<string> PostAsync<T>(string url, T type)
        {
            var message = await ServiceRepository.PostAsync<T>(url, type).ConfigureAwait(false);
            return message;
        }


        #endregion

        #region Test Autologin
        public ActionResult TeacherAutoLogin(string AutoLoginDetails)
        {
            string teacherSSOEncryptionKey = "";
            string teacherSSOErrorURL = "";

            try
            {
                teacherSSOEncryptionKey = Convert.ToString(ConfigurationManager.AppSettings["TeacherSSOEncryptionKey"]);
                teacherSSOErrorURL = Convert.ToString(ConfigurationManager.AppSettings["TeacherSSOErrorURL"]);

                string AutoLoginEmailDetailsDecypted = EncryptDecryptUtility.Decrypt(AutoLoginDetails, teacherSSOEncryptionKey);
                string email = "";
                string pwd = "";
                string ErrorMessage = "";
                bool returnError = false;

                LogtoLocalFile("AutoLoginEmailDetailsDecypted", AutoLoginEmailDetailsDecypted);

                User userobj = new User();
                LogtoLocalFile("userobj 1", JsonConvert.SerializeObject(userobj));
                userobj.Email = AutoLoginEmailDetailsDecypted;

                LogtoLocalFile("userobj", JsonConvert.SerializeObject(userobj));
                var response = PostAsync("Account/FindTeacherByEmail", userobj);
                var matchingUsers = JsonConvert.DeserializeObject<System.Collections.Generic.List<User>>(response.Result);


                LogtoLocalFile("response.Result", JsonConvert.SerializeObject(response.Result));

                if (matchingUsers==null)
                {
                    ErrorMessage += "Could not find matching user";
                    returnError = true;
                }
                if (matchingUsers.Count==0)
                {
                    ErrorMessage += "Could not find matching user";
                    returnError = true;
                }

                if (matchingUsers.Count > 1)
                {
                    ErrorMessage += "More than one matching users found";
                    returnError = true;
                }


                LogtoLocalFile("ErrorMessage", JsonConvert.SerializeObject(ErrorMessage));

                if (!returnError)
                {

                    LogtoLocalFile("matchingUsers", JsonConvert.SerializeObject(matchingUsers));
                    var userModel = new User();
                    userModel = matchingUsers[0];

                    LogtoLocalFile("userModel", JsonConvert.SerializeObject(userModel));

                    //Log user session in database
                    UserSession userSession = new UserSession();
                    userSession.UserId = userModel.UserId;
                    var sessionResponse = PostAsync("Account/SaveUserSession", userSession);
                    var guid = JsonConvert.DeserializeObject<UserSession>(sessionResponse.Result);


                    //If the User is valid Set Authentication and redirect to the eBooks list page
                    FormsAuthentication.SetAuthCookie(userModel.Email, false);
                    System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie(
                        FormsAuthentication.FormsCookieName,
                        FormsAuthentication.Encrypt(new FormsAuthenticationTicket(1, userModel.Email, DateTime.Now,
                            DateTime.Now.AddMinutes(20160), false, ""))));
                    Session["District"] = userModel.District;
                    Session["Grade"] = userModel.Grade;
                    Session["Email"] = userModel.Email;
                    Session["guid"] = guid.access_token;
                    Session["UserID"] = userModel.UserId;
                    Session["UserEmail"] = userModel.Email;

                    //set cookie for other domain.                
                    string cookieSeparator = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieSeparator"]) ? "||" : ConfigurationManager.AppSettings["SwunmathCookieSeparator"];
                    string cookieName = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieName"]) ? "teacheridentifier" : ConfigurationManager.AppSettings["SwunmathCookieName"];
                    string cookieDomain = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieDomain"]) ? "swunmath.com" : ConfigurationManager.AppSettings["SwunmathCookieDomain"];
                    string cookieExpiresMinutes = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwunmathCookieExpires"]) ? "60" : ConfigurationManager.AppSettings["SwunmathCookieExpires"];

                    string cookieValue = userModel.Email + cookieSeparator + guid.access_token; // vasim@strategybeach.com||d49f9d07-d095-11ea-8554-f23c91181be8
                    string base64EncodedCookieValue = Common.ConvertStringToBase64String(cookieValue);

                    HttpCookie httpCookie = new HttpCookie(cookieName);
                    httpCookie.Value = base64EncodedCookieValue; // "hello";
                    httpCookie.Domain = cookieDomain; //"microsoft.com";
                    httpCookie.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(cookieExpiresMinutes));
                    httpCookie.Secure = true;

                    Response.Cookies.Add(httpCookie);
                    Response.Write(Request.Cookies[cookieName].Value);
                    //end

                    #region Test Cookie
                    //set cookie for other domain.                

                    var newCookieName = cookieName + "_new";

                    HttpCookie httpCookieNew = new HttpCookie(newCookieName);
                    httpCookieNew.Value = base64EncodedCookieValue; // "hello";
                    httpCookieNew.Domain = cookieDomain; //"microsoft.com";
                    httpCookieNew.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(cookieExpiresMinutes));
                    //httpCookieNew.Secure = true;

                    Response.Cookies.Add(httpCookieNew);
                    Response.Write(Request.Cookies[newCookieName].Value);
                    //end
                    #endregion

                    Session["token"] = base64EncodedCookieValue;

                    Log.Info("Valid Credentials");

                    UserDal _user = new UserDal();

                    int userTypeId = _user.GetClassroomUserTypeIdEmail(userModel.Email);

                    Log.Info("UserTypeId = " + userTypeId);

                    if (userTypeId == 4)
                    {
                        return Redirect(ConfigurationManager.AppSettings["ClassroomDistrictAdminRedirectURL"]);
                    }
                    else if (userTypeId == 5)
                    {
                        return Redirect(ConfigurationManager.AppSettings["ClassroomSchoolAdminRedirectURL"]);
                    }
                    else
                    {
                        LogToFile.Log("Account/TeacherLogin[Post]", "Redirect to Resources/EBooksList", "URL=", Convert.ToString(Request.Url),
                            "RefURL=", Convert.ToString(Request.UrlReferrer));

                        if (Request.Url.Host != "localhost")
                        {
                            UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                            return Redirect(u.Action("EBooksList", "Resources", null, "https"));
                        }
                        else
                        {
                            return RedirectToAction("EBooksList", "Resources");
                        }
                    }

                }
                else
                {
                    string ErrorMessageListingURL = teacherSSOErrorURL + "" + ErrorMessage;
                    return Redirect(ErrorMessageListingURL);
                }
            }
            catch (Exception ex)
            {
                if (teacherSSOErrorURL == null || teacherSSOErrorURL == "")
                {
                    return View("_Error");
                }
                else
                {
                    //return View("_Error");
                    LogtoLocalFile("Error", ex.Message.ToString());
                    string ErrorMessageListingURL = teacherSSOErrorURL + "Error While Login (SSO)";
                    return Redirect(ErrorMessageListingURL);
                }
            }
        }


        public void LogtoLocalFile(params string[] LogStrings)
        {

            // LogToFile.Log("userForOAuthVerificationResult", JsonConvert.SerializeObject(userForOAuthVerificationResult)); Exam ple Call
            string LogFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FileLogFolder"]);
            string FileLogYN = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FileLogYN"]);
            if (LogFolder != null && (FileLogYN.Trim().ToUpper() == "Y" || FileLogYN.Trim().ToUpper() == "YES"))
            {
                try
                {


                    if (!System.IO.Directory.Exists(LogFolder))
                    {
                        System.IO.Directory.CreateDirectory(LogFolder);
                    }

                    string logFile = DateTime.Now.ToString("ddMMyyyy") + "_TeacherPortal.txt";
                    string logFilefullpath = LogFolder + "\\" + logFile;

                    if (!System.IO.File.Exists(logFilefullpath))
                    {
                        System.IO.FileStream f = System.IO.File.Create(logFilefullpath);
                        f.Close();
                    }

                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(logFilefullpath, true))
                    {
                        sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF"));
                        sw.WriteLine(string.Join(" - ", LogStrings));
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {

                }


            }
        }


        [HttpGet]
        public bool UpdateMessageSeen()
        {
            string UserEmail = Convert.ToString(Session["Email"]);

            var response = PostAsync("Account/UpdateNoticeBoardMessageSeen", UserEmail);
            var HasUpdated = Convert.ToBoolean(response.Result);

            return HasUpdated;
        }



        [HttpGet]
        public bool GetNoticeBoardMessages()
        {
            string UserEmail = Convert.ToString(Session["Email"]);

            var response = PostAsync("Account/FetchNoticeBoardMessages", UserEmail);
            var HasUpdated = Convert.ToBoolean(response.Result);

            return HasUpdated;
        }



        [HttpGet]
        public bool GetNoticeBoardMessagesFlag()
        {
            string UserEmail = Convert.ToString(Session["Email"]);

            var response = PostAsync("Account/FetchNoticeBoardMessageSeen", UserEmail);
            var HasUpdated = Convert.ToBoolean(response.Result);

            return HasUpdated;
        }


        #endregion
    }
}