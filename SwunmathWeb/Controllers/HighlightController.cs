﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Swunmath.Dal;
using Newtonsoft.Json;

namespace SwunmathWeb.Controllers
{
    public class HighlightController : ApiController
    {
        [HttpPost, HttpOptions]
        [Route("~/api/Highlight/GetHighlights/{UserID}")]
        public IEnumerable<Highlight> GetHighlights(string UserID)
        {
            try
            {
                string BookID = "";
                var bodyStream = new System.IO.StreamReader(System.Web.HttpContext.Current.Request.InputStream);
                bodyStream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
                var bodyText = bodyStream.ReadToEnd();
                BookID = bodyText;

                string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
                System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
                System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

                //var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                //UserID = context.Session["UserID"].ToString();

                using (var context = new SWUNMATHEntities())
                {
                    return context.Highlights.Where(X => X.UserName == UserID && X.BookID == BookID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, HttpOptions]
        [Route("~/api/Highlight/SaveHighlight")]
        public int SaveHighlight()
        {
            int HighlightId = 0;

            string inputdetails_main = "", inputdetails = "";
            var bodyStream = new System.IO.StreamReader(System.Web.HttpContext.Current.Request.InputStream);
            bodyStream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();

            try
            {
                bodyText = bodyText.TrimEnd(':');
                Highlight highlight = JsonConvert.DeserializeObject<Highlight>(bodyText);

                using (var dbContext = new SWUNMATHEntities())
                {
                    highlight.UserName = highlight.UserId;
                    dbContext.Highlights.Add(highlight);
                    dbContext.SaveChanges();
                    HighlightId = dbContext.Highlights.Max(X => X.HighlightID);
                }
            }
            catch (Exception ex)
            {
                inputdetails_main = bodyText + ex.Message.ToString();
            }

            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            return HighlightId;
        }

        [HttpDelete, HttpOptions]
        [Route("~/api/Highlight/DeleteHighlight/{HId}")]
        public bool DeleteHighlight(int HId)
        {
            using (var context = new SWUNMATHEntities())
            {
                Highlight HLight = context.Highlights.Where(X => X.HighlightID == HId).SingleOrDefault();

                if (HLight != null)
                {
                    context.Highlights.Remove(HLight);
                    context.SaveChanges();
                }
            }

            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            return true;
        }
    }
}

