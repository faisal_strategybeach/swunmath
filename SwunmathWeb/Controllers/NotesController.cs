﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Swunmath.Dal;
using Newtonsoft.Json;

namespace SwunmathWeb.Controllers
{
    public class NotesController : ApiController
    {
        [HttpPost, HttpOptions]
        [Route("~/api/Notes/GetNotes/{UserID}")]
        public IEnumerable<Note> GetNotes(int UserId)
        {
            try
            {
                string BookID = "";
                var bodyStream = new System.IO.StreamReader(System.Web.HttpContext.Current.Request.InputStream);
                bodyStream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
                var bodyText = bodyStream.ReadToEnd();
                BookID = bodyText;

                string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
                System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
                System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

                using (var dbContext = new SWUNMATHEntities())
                {
                    var Notes = dbContext.Notes.Where(X => (X.UserName == UserId.ToString() && X.BookID == BookID)).ToList();
                    Notes = Notes.OrderBy(x => x.NoteID).ToList();
                    return Notes;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, HttpOptions]
        [Route("~/api/Notes/SaveNotes")]
        public int SaveNotes()
        {
            int NoteID = 1;

            string inputdetails_main = "", inputdetails = "";
            var bodyStream = new System.IO.StreamReader(System.Web.HttpContext.Current.Request.InputStream);
            bodyStream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();

            try
            {
                bodyText = bodyText.TrimEnd(':');
                Note note = JsonConvert.DeserializeObject<Note>(bodyText);

                using (var dbContext = new SWUNMATHEntities())
                {
                    note.UserName = note.UserId;
                    dbContext.Notes.Add(note);
                    dbContext.SaveChanges();
                    NoteID = dbContext.Notes.Max(X => X.NoteID);
                }
            }
            catch (Exception ex)
            {
                inputdetails_main = bodyText + ex.Message.ToString();
            }

            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            return NoteID;
        }

        [HttpDelete, HttpOptions]
        [Route("~/api/Notes/DeleteNote/{NoteID}")]
        public bool DeleteNote(int NoteID)
        {
            using (var context = new SWUNMATHEntities())
            {
                Note note = context.Notes.Where(X => X.NoteID == NoteID).SingleOrDefault();

                if (note != null)
                {
                    context.Notes.Remove(note);
                    context.SaveChanges();
                }
            }
            return true;
        }

        [HttpPut, HttpOptions]
        [Route("~/api/Notes/UpdateNoteIconPosition/{NoteID}/{IconPos}")]
        public bool UpdateNoteIconPosition(int NoteID, string IconPos)
        {
            IconPos = Common.ConvertBase64StringToString(IconPos);

            using (var context = new SWUNMATHEntities())
            {
                Note note = context.Notes.Where(X => X.NoteID == NoteID).SingleOrDefault();

                if (note != null)
                {
                    note.IconPosition = IconPos;
                    context.SaveChanges();
                }
            }

            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            return true;
        }
    }
}
