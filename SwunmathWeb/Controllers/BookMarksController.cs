﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Swunmath.Dal;
using Swunmath.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using SwunmathWeb.Services;

namespace SwunmathWeb.Controllers
{
    public class BookMarksController : ApiController
    {

        [HttpGet]
        [Route("~/api/Bookmarks/GetEbookInitDetails/{UId}/{token}/{ebookId}")]
        public string GetEbookInitDetails(int UId, string token, int ebookId)
        {
            int UserId = UId;
            string EbookAccessToken = "";
            string EbookConfig = "";

            UserSession request = new UserSession();
            request.UserId = UId;
            request.access_token = EbookAccessToken;

            UserDal _userDal = new UserDal();

            UserSession userSession = new UserSession();
            userSession.UserId = UId;
            var sessionResponse = PostAsync("Account/SaveUserSession", userSession);
            var guid = JsonConvert.DeserializeObject<UserSession>(sessionResponse.Result);

            EbookAccessToken = guid.access_token;

            if (token.Trim().ToLower() == EbookAccessToken.Trim().ToLower())
            {

                using (var dbContext = new SWUNMATHEntities())
                {
                    var ebook = dbContext.eBooks.Where(X => X.Id == ebookId).FirstOrDefault();

                    EbookConfig = ebook?.ConfigString;
                }
            }

            string urlProtocol = System.Web.HttpContext.Current.Request.Url.Scheme.ToLower();
            string host = System.Web.HttpContext.Current.Request.Url.Host;

            string CurrentURL = "https://" + host;

            if (host.Contains("localhost")) 
            {
                CurrentURL = "http://localhost:55229";
            }

            GetEbookInitDetailsResponse getEbookInitDetailsResponse = new GetEbookInitDetailsResponse();
            getEbookInitDetailsResponse.UserId = UserId;
            getEbookInitDetailsResponse.CurrrentDomain = CurrentURL;
            getEbookInitDetailsResponse.eBookConfig = EbookConfig;


            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            string returnval = JsonConvert.SerializeObject(getEbookInitDetailsResponse);
            return returnval;
        }

        [HttpGet]
        [Route("~/api/Bookmarks/GetBookmarks_old/{UserID}/{BookID}")]
        public IEnumerable<Bookmark> GetBookmarks_old(string UserID, string BookID)
        {
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                UserID = context.Session["UserID"].ToString();
            }
            catch (Exception) { }

            using (var dbContext = new SWUNMATHEntities())
            {
                return dbContext.Bookmarks.Where(X => X.UserName == UserID && X.BookID == BookID).ToList();
            }
        }

        [HttpPost, HttpOptions]
        [Route("~/api/Bookmarks/GetBookmarks/{UserID}")]
        public IEnumerable<Bookmark> GetBookmarks(string UserID)
        {

            string BookID = "";
            var bodyStream = new System.IO.StreamReader(System.Web.HttpContext.Current.Request.InputStream);
            bodyStream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();
            BookID = bodyText;

            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            using (var dbContext = new SWUNMATHEntities())
            {
                return dbContext.Bookmarks.Where(X => X.UserName == UserID && X.BookID == BookID).ToList();
            }
        }

        [HttpPost]
        [Route("~/api/Bookmarks/SaveBookMark_Old")]
        public int SaveBookMark_Old([FromBody] Bookmark bookmark)
        {
            int BookmarkID = 0;
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;

                using (var dbContext = new SWUNMATHEntities())
                {
                    bookmark.UserName = context.Session["UserID"].ToString();
                    dbContext.Bookmarks.Add(bookmark);
                    dbContext.SaveChanges();
                    BookmarkID = dbContext.Bookmarks.Max(X => X.BookmarkID);
                }
            }
            catch (Exception) { }

            return BookmarkID;
        }

        [HttpPost, HttpOptions]
        [Route("~/api/Bookmarks/SaveBookMark")]
        public int SaveBookMark()
        {
            int BookmarkID = 0;

            string inputdetails_main = "", inputdetails = "";
            var bodyStream = new System.IO.StreamReader(System.Web.HttpContext.Current.Request.InputStream);
            bodyStream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();
            try
            {
                bodyText = bodyText.TrimEnd(':');
                Bookmark bookmark = JsonConvert.DeserializeObject<Bookmark>(bodyText);
                
                using (var dbContext = new SWUNMATHEntities())
                {
                    bookmark.UserName = bookmark.UserId;
                    dbContext.Bookmarks.Add(bookmark);
                    dbContext.SaveChanges();
                    BookmarkID = dbContext.Bookmarks.Max(X => X.BookmarkID);
                }
                //string Hlight = bodyText;
            }
            catch (Exception ex)
            {
                inputdetails_main = bodyText + ex.Message.ToString();
            }


            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);
            //string Hlight = Convert.ToString(inputdetails_main) + "test";
            return BookmarkID;
        }

        [HttpDelete]
        [Route("~/api/Bookmarks/DeleteBookMark/{BmID}")]
        public bool DeleteBookmark(int BmID)
        {
            using (var context = new SWUNMATHEntities())
            {
                Bookmark BM = context.Bookmarks.Where(X => X.BookmarkID == BmID).SingleOrDefault();

                if (BM != null)
                {
                    context.Bookmarks.Remove(BM);
                    context.SaveChanges();
                }
            }

            string eBookDomain = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eBookBaseUrl"]);
            System.Web.HttpContext.Current.Response.Headers.Remove("Access-Control-Allow-Origin");
            System.Web.HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", eBookDomain);

            return true;
        }

        private static async Task<string> PostAsync<T>(string url, T type)
        {
            var message = await ServiceRepository.PostAsync<T>(url, type).ConfigureAwait(false);
            return message;
        }
    }

    public class GetEbookInitDetailsResponse
    {
        public int UserId { get; set; }
        public string CurrrentDomain { get; set; }
        public string eBookConfig { get; set; }

    }
}