﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Swunmath.Dal;
using Newtonsoft.Json.Linq;

namespace SwunmathWeb.Controllers
{
    public class eBookController : ApiController
    {
        [HttpGet]
        [Route("~/api/eBook/GeteBooks/{UserID}")]
        public List<eBook> GeteBooks(int UserId)
        {
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                UserId = Convert.ToInt32(context.Session["UserEmail"].ToString());
            }
            catch (Exception) { }

            using (var context = new SWUNMATHEntities())
            {
                var eBooks = context.eBooks.Where(x => x.Status == true).ToList();
                return eBooks;
            }
        }

        [HttpPost]
        [Route("~/api/eBook/Drawings/SaveScreen")]
        public Drawings SaveScreen([FromBody]Drawings drawings)
        {
            try
            {
                var sessionContext = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                drawings.user = sessionContext.Session["UserEmail"].ToString();

                int eBookId = GeteBookId(drawings.pdfPath);
                if (!string.IsNullOrEmpty(drawings.book))
                {
                    drawings.book = drawings.book.Replace('!', ':');
                }
                //drawings.user = "manjulaagnihotri19771@gmail.com";
                using (var context = new SWUNMATHEntities())
                {
                    var drwaings = context.DrawingScreens.Where(x => x.user == drawings.user && x.book == drawings.book && x.chapter == drawings.chapter && x.eBookId == eBookId && x.DrawingToolbarPath == drawings.DrawingToolbarPath).FirstOrDefault();
                    if (drwaings != null)
                    {
                        context.DrawingScreens.Remove(drwaings);
                        context.SaveChanges();
                    }
                    if (drawings != null && drawings.drawing != null)
                    {
                        var drwaingboard = new DrawingScreen();
                        drwaingboard.book = drawings.book;
                        drwaingboard.chapter = drawings.chapter;
                        drwaingboard.drawing = Convert.ToString(drawings.drawing);
                        //drwaingboard.Id = drawings.Id;
                        drwaingboard.eBookId = eBookId;
                        drwaingboard.user = drawings.user;
                        drwaingboard.DrawingToolbarPath = drawings.DrawingToolbarPath;
                        context.DrawingScreens.Add(drwaingboard);
                        context.SaveChanges();
                        //drwaings.Id= context.DrawingScreens.OrderByDescending(x => x.user == drawings.user && x.book == drawings.book && x.chapter == drawings.chapter && x.eBookId == eBookId).FirstOrDefault().Id;
                    }
                }
            }
            catch (Exception ex) { }
            
            return drawings;
        }

        [HttpGet]
        [Route("~/api/eBook/Drawings/LoadScreen/{userMail}/{bookUUID}/{chapterPath}/{pdfPath}/{drawingToolbarPath}")]
        public Drawings LoadScreen(string userMail, string bookUUID, string chapterPath, string pdfPath, string drawingToolbarPath)
        {
            var drwaingboard = new Drawings();
            try
            {
                var sessionContext = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                userMail = sessionContext.Session["UserEmail"].ToString();

                int eBookId = GeteBookId(pdfPath);
                if (!string.IsNullOrEmpty(userMail))
                    userMail = userMail.Replace('~', '.');
                
                if (!string.IsNullOrEmpty(drawingToolbarPath))
                    drawingToolbarPath = drawingToolbarPath.Replace('~', '.');
                
                if (!string.IsNullOrEmpty(bookUUID))
                    bookUUID = bookUUID.Replace('!', ':');
                
                if (!string.IsNullOrEmpty(chapterPath))
                    chapterPath = chapterPath.Replace('$', '/').Replace('~', '.');
                
                using (var context = new SWUNMATHEntities())
                {
                    var drwaings = context.DrawingScreens.Where(x => x.user == userMail && x.book == bookUUID && x.chapter == chapterPath && x.eBookId == eBookId && x.DrawingToolbarPath == drawingToolbarPath).FirstOrDefault();

                    if (drwaings != null && drwaings.drawing != null)
                    {
                        drwaingboard.Id = drwaings.Id;
                        drwaingboard.book = drwaings.book;
                        drwaingboard.chapter = drwaings.chapter;
                        drwaingboard.drawing = JObject.Parse(drwaings.drawing);
                        drwaingboard.Id = drwaings.Id;
                    }
                }
            }
            catch (Exception ex) { }
            
            return drwaingboard;
        }

        private int GeteBookId(string PdfPath)
        {
            int eBookId = 0;
            using (var context = new SWUNMATHEntities())
            {
                var drwaings = context.eBooks.Where(x => x.PdfPath == PdfPath).FirstOrDefault();
                if (drwaings != null)
                {
                    eBookId = drwaings.Id;
                }
            }
            return eBookId;
        }


        public class Drawings
        {
            public int Id { get; set; }
            public string book { get; set; }
            public string chapter { get; set; }
            public dynamic drawing { get; set; }
            public Nullable<System.DateTime> createdon { get; set; }
            public Nullable<System.DateTime> updatedon { get; set; }
            public string user { get; set; }
            public string pdfPath { get; set; }
            public string DrawingToolbarPath { get; set; }

        }
    }
}